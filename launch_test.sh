#/bin/sh
rm -rf temporary
ln -fs ../data .
mkdir -p temporary/binary
mkdir -p temporary/ascii
mkdir -p temporary/accumulateBinary
export OBADES_CONFIGURATION=data//obadesConfig.json
echo "-=-=-=-=-=-=-=- obades NR test -=-=-=-=-=-=-=-=-=-=-=-=-"
./src/exe/obades
# cat temporary/ascii/mcdf.don_010000A
diff data/ascii/03/01/0103/mcdfDon/mcdf.don_Met1_03_01_0103_010000000000000000000000000000000000000000 temporary/ascii/03/01/0103/mcdfDon/mcdf.don_Met9_03_01_0103_010000000000000000000000000000000000000000
diff data/ascii/03/02/0103/mcdfDon/mcdf.don_Met3_03_02_0103_010000000000000000010000000000000000000000 temporary/ascii/03/02/0103/mcdfDon/mcdf.don_Met9_03_02_0103_010000000000000000010000000000000000000000A
diff data/ascii/03/02/0103/mcdfDon/mcdf.don_Met3_03_02_0103_010000000000000000010000000000000000000000 temporary/ascii/03/02/0103/mcdfDon/mcdf.don_Met9_03_02_0103_010000000000000000010000000000000000000000C
rm -r temporary/accumulateBinary
mkdir -p temporary/accumulateBinary
echo "-=-=-=-=-=-=-=- First ascii_to_binary run  -=-=-=-=-=-=-"
./src/tools/ascii_to_binary
echo "-=-=-=-=-=-=-=- Second ascii_to_binary run -=-=-=-=-=-=- "
./src/tools/ascii_to_binary
ierr=$?
echo "ierr: "$ierr
if test $ierr -eq  1
then
    echo "It's normal. The second call /src/tools/ascii_to_binary produce FATAL ERROR."
else
    echo "It's NOT normal. The second call /src/tools/ascii_to_binary MUST BE PRODUCE FATAL ERROR."
fi
echo "-=-=-=-=-=-=-=- extract_McdfDescription  -=-=-=-=-=-=-=-=-=-"
./src/tools/extract_McdfDescription
echo "-=-=-=-=-=-=-=-extract_McdfParameters -=-=-=-=-=-=-=-=-=-"
./src/tools/extract_McdfParameters
echo "-=-=-=-=-=-=-=- extract_McdfDb -=-=-=-=-=-=-=-=-=-=-=-=-=-"
./src/tools/extract_McdfDb

echo "-=-=-=-=-=-=-=- mcdf_for_saphyr -=-=-=-=-=-=-=-=-=-=-=-=-=-"
./src/exe/test_mcdf_for_saphyr
ierr=$?
echo "ierr: "$ierr
if test $ierr -eq  1
then
    echo "test_mcdf_for_saphyr Failed"
else
    echo "test_mcdf_for_saphyr Successful"
fi

