// obades.hpp

#ifndef OBADES_H
#define OBADES_H

#include <cassert>
#include <cstdlib>
#include <ctime>
#include <ios>
#include <iostream>
#include <filesystem>
#include <fstream>
#include <functional>
#include <limits>
#include <memory>
#include <set>
#include <string>
#include <vector>

#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "json.hpp"
using json = nlohmann::json;

//-------------------------------------------------------------------------------------------------
// BeforeC++20 (see C++20) https://stackoverflow.com/questions/2342162/stdstring-formatting-like-sprintf

namespace
{
    /**
     * @brief Méthode pour formater les données écrites dans une string
     */
    template <typename... Args>
    std::string string_format(const std::string &format, Args... args)
    {
        int size_s = std::snprintf(nullptr, 0, format.c_str(), args...) + 1; // Extra space for '\0'
        if (size_s <= 0)
        {
            throw std::runtime_error("Error during formatting.");
        }
        auto size = static_cast<size_t>(size_s);
        std::unique_ptr<char[]> buf(new char[size]);
        std::snprintf(buf.get(), size, format.c_str(), args...);
        return std::string(buf.get(), buf.get() + size - 1); // We don't want the '\0' inside
    }
}

//-------------------------------------------------------------------------------------------------

namespace obades
{
     //-------------------------------------------------------------------------------------------------
    /**
     * @brief Namespace obades
     *
     * Ce namespace a son propre mécanisme de gestion des traces, des logs.
     * La macro TRACE permet d'afficher des traces en std::cout si elles sont actives.
     * Les macros ERROR et GURU permettent d'afficher des traces d'erreur ou d'erreur
     * interne (d'où l'aspect GURU MEDITATION) en std::cerr.
     * Ordre de positionnement de l'activation ou non des traces :
     * - le défaut est false,
     * - mais à la construction de obades, on peut piloter et changer ce défaut
     * - mais on peut aussi piloter et changeer ce défaut au lancement à travers
     *   la variable d'environnement OBADES_TRACE qui peut prendre la valeur "1" (true)
     *   ou "0" (false) ; dans les faits, seul "0" correspond à false et toutes
     *   les autres valeurs à true.
     **/

    bool trace {false};

#define SET_TRACE(val) trace = val;

#define ERROR(msg) std::cerr << "#### ERROR #### " << msg << "\n";

#define GURU(msg) std::cerr << "#### GURU MEDITATION #### " << msg << "\n";

#define TRACE(msg)                \
    if (trace)            \
    {                             \
        std::cout << msg << "\n"; \
    }

#define RETURN(msg, value)                         \
    {                                              \
        auto _value = value;                       \
        TRACE("RETURN " << msg << ": " << _value); \
        return _value;                             \
    }

    //-------------------------------------------------------------------------------------------------
    /**
     * @brief Classe de gestion de bases OBaDES
     *
     * La classe obades ne comporte en mémoire qu'un Record à la fois.
     * Tout chargement d'un nouveau Record se traduit par l'écrasement du précédent.
     * TODO On pourrait envisager de mettre en place un cache (à définir la taille) si le code
     * TODO appelant est amené à charger régulièrement un même Record (c'est une intervention à
     * TODO faire dans cette classe qui permettra d'éviter dans une certaine mesure le rechargement
     * TODO d'enregistrement).
     *
     * ERROR_DB_ASCII_FORMAT: incoherence entre le nombre de raies attendu et ce qui est lu
     */
    class obades
    {
    public:
        enum ERRORS
        {
            NO_ERROR = 0,
            ERROR_DB_FAILED,
            ERROR_DB_ASCII_FILE_NOT_EXIST,
            ERROR_DB_ASCII_FILE_ALREADY_EXIST,
            ERROR_DB_ASCII_FORMAT,
            ERROR_PARAMETERS_FILE_NOT_EXIST,
            ERROR_PARAMETERS_FILE_EMPTY,
            ERROR_ORIGINAL_INDEX_ACCUMULATE_BINARY_FILE_NOT_EXIST,
            ERROR_INDEX_ACCUMULATE_BINARY_FILE_NOT_EXIST,
            ERROR_DB_ACCUMULATE_BINARY_FILE_NOT_EXIST,
            ERROR_DB_ACCUMULATE_BINARY_ALREADY_EXIST,
            ERROR_INVALID_CHECKSUM
        };

    private:

        /**
         * @brief Classe de description d'un Record
         * - forcOsc: ...
         * - un tableau de raies de type Raie
         */
        class Record
        {
        private:
            /**
             * @brief Classe de description d'une Raie
             * - enerRy: ...
             * - proba: ...
             * - J21Ini: ...
             * - J21Fin: ...
             */
            class Raie
            {
            public:
                /**
                 * @brief Constructeur
                 */
                Raie(){};

                /**
                 * @brief Destructeur avec initialisation
                 */
                Raie(const float _enerRy, const float _proba, const unsigned char _J21Ini, const unsigned char _J21Fin)
                    : m_enerRy(_enerRy), m_proba(_proba), m_J21Ini(_J21Ini), m_J21Fin(_J21Fin){};

                /**
                 * @brief Destructeur
                 */
                ~Raie(){};

                /**
                 * @brief Setter
                 */
                void SetEnerRy(const float _enerRy) { m_enerRy = _enerRy; }
                void SetProba(const float _proba) { m_proba = _proba; }
                void SetJ21Ini(const unsigned char _J21Ini) { m_J21Ini = _J21Ini; }
                void SetJ21Fin(const unsigned char _J21Fin) { m_J21Fin = _J21Fin; }

                /**
                 * @brief Getter
                 */
                float GetEnerRy() const { return m_enerRy; }
                float GetProba() const { return m_proba; }
                unsigned char GetJ21Ini() const { return m_J21Ini; }
                unsigned char GetJ21Fin() const { return m_J21Fin; }

                /**
                 * @brief getSize
                 * @return ...
                 */
                static size_t getSize()
                {
                    return sizeof(float) + sizeof(float) + sizeof(unsigned char) + sizeof(unsigned char);
                }

            private:
                float m_enerRy = 0.;
                float m_proba = 0.;
                unsigned char m_J21Ini = 0;
                unsigned char m_J21Fin = 0;
            };

        public:
            /**
             * @brief Constructeur
             */
            Record(){};

            /**
             * @brief Destructeur
             */
            ~Record(){};

            /**
             * @brief Réinitialisation de l'enregistrement
             */
            void clear()
            {
                m_forcOsc = 0.; // TODO Mettre une valeur defaut plus significativement incorrect
                m_raies.clear();
                m_checksum_data = 0;
                m_checksum_all = 0;
                m_description = "";
                m_configuration = "";
            }

            /**
             * @brief Set ...
             * @param _forcOsc: ...
             */
            void setForcOsc(const double _forcOsc) { m_forcOsc = _forcOsc; }

            /**
             * @brief Requests that the vector capacity be at least enough to contain _reserve elements
             * @param _reserve: ...
             */
            void reserveRaies(std::size_t _reserve)
            {
                m_raies.reserve(_reserve);
            }

            /**
             * @brief Add a Raie
             * @param _enerRy: ...
             * @param _proba: ...
             * @param _J21Ini: ...
             * @param _J21Fin: ...
             */
            void appendRaie(const float _enerRy, const float _proba, const unsigned char _J21Ini, const unsigned char _J21Fin)
            {
                m_raies.emplace_back(Raie(_enerRy, _proba, _J21Ini, _J21Fin));
            }

            /**
             * @brief Get ...
             * @return ...
             */
            int getForcOsc(double &_forcOsc) const
            {
                _forcOsc = m_forcOsc;
                return 0;
            }

            /**
             * @brief Get number of Raie
             * @return ...
             */
            size_t getNumberOfRaies() const
            {
                return m_raies.size();
            }

            /**
             * @brief Get a Raie
             * @param _index: ...
             * @param _enerRy (out): ...
             * @param _proba (out): ...
             * @param _J21Ini (out): ...
             * @param _J21Fin (out): ...
             */
            int getRaie(const size_t _index, float &_enerRy, float &_proba, unsigned char &_J21Ini, unsigned char &_J21Fin) const
            {
                if (_index >= m_raies.size())
                {
                    return 1;
                }
                const Raie &raie = m_raies.at(_index);
                _enerRy = raie.GetEnerRy();
                _proba = raie.GetProba();
                _J21Ini = raie.GetJ21Ini();
                _J21Fin = raie.GetJ21Fin();
                return 0;
            }

            /**
             * @brief Get all Raies
             * @param _enerRy (out): ...
             * @param _proba (out): ...
             * @param _J21Ini (out): ...
             * @param _J21Fin (out): ...
             */
            int getRaies(std::vector<float> &_enerRy, std::vector<float> &_proba, std::vector<unsigned char> &_J21Ini, std::vector<unsigned char> &_J21Fin) const
            {
                std::size_t taille = std::size(m_raies);
                _enerRy.reserve(taille);
                _proba.reserve(taille);
                _J21Ini.reserve(taille);
                _J21Fin.reserve(taille);
                for (const Raie &raie : m_raies)
                {
                    _enerRy.emplace_back(raie.GetEnerRy());
                    _proba.emplace_back(raie.GetProba());
                    _J21Ini.emplace_back(raie.GetJ21Ini());
                    _J21Fin.emplace_back(raie.GetJ21Fin());
                }
                return 0;
            }
            int getRaies(std::vector<float> &_enerRy, std::vector<float> &_proba, std::vector<int> &_J21Ini, std::vector<int> &_J21Fin) const
            {
                std::size_t taille = std::size(m_raies);
                _enerRy.reserve(taille);
                _proba.reserve(taille);
                _J21Ini.reserve(taille);
                _J21Fin.reserve(taille);
                for (const Raie &raie : m_raies)
                {
                    _enerRy.emplace_back(raie.GetEnerRy());
                    _proba.emplace_back(raie.GetProba());
                    _J21Ini.emplace_back(raie.GetJ21Ini());
                    _J21Fin.emplace_back(raie.GetJ21Fin());
                }
                return 0;
            }

            /**
             * @brief Get all Raies
             * @param _energyMin (in): ...
             * @param _energyMax (in): ...
             * @param _enerRy (out): ...
             * @param _proba (out): ...
             * @param _J21Ini (out): ...
             * @param _J21Fin (out): ...
             */
            int getRaies(const float _energyMin, const float _energyMax, std::vector<float> &_enerRy, std::vector<float> &_proba, std::vector<unsigned char> &_J21Ini, std::vector<unsigned char> &_J21Fin) const
            {
                bool finded = false;
                std::size_t taille = std::size(m_raies);
                // on alloue au nombre de raies mais on ne renseigne que le juste nécessaire
                _enerRy.reserve(taille);
                _proba.reserve(taille);
                _J21Ini.reserve(taille);
                _J21Fin.reserve(taille);
                for (const Raie &raie : m_raies)
                {
                    float energy = raie.GetEnerRy();
                    if (_energyMin <= energy && energy <= _energyMax)
                    {
                        finded = true;
                        _enerRy.emplace_back(energy);
                        _proba.emplace_back(raie.GetProba());
                        _J21Ini.emplace_back(raie.GetJ21Ini());
                        _J21Fin.emplace_back(raie.GetJ21Fin());
                    }
                    else if (finded)
                    {
                        // ernerRy est rangé par ordre croissant
                        // ce qui fait qu'on va d'abord rencontrer ignorer puis energyMin puis energyMax puis ignorer
                        // le fait de sortir de depasser energyMax entraine le break
                        break;
                    }
                }
                return 0;
            }

            /**
             * @brief Get moment
             * m1 = somme (proba * energy) / somme(proba)
             * m2 = somme (proba * (energy - m1)^2) / somme(proba)
             * m3 = somme (proba * (energy - m1)^3) / somme(proba)
             * @param _m1 (out): ...
             * @param _m2 (out): ...
             * @param _m3 (out): ...
             */
            int getMoment(float &_m1, float &_m2, float &_m3) const
            {
                float sum_proba = 0.;
                float sum_proba_x_energy = 0.;
                for (const Raie &raie : m_raies)
                {
                    float energy = raie.GetEnerRy();
                    float proba = raie.GetProba();
                    sum_proba += proba;
                    sum_proba_x_energy += proba * energy;
                }
                _m1 = sum_proba_x_energy / sum_proba;
                float sum_proba_x_carre_diff_energy_m1 = 0.;
                float sum_proba_x_cube_diff_energy_m1 = 0.;
                for (const Raie &raie : m_raies)
                {
                    float energy = raie.GetEnerRy();
                    float proba = raie.GetProba();
                    sum_proba_x_carre_diff_energy_m1 += proba * std::pow(energy - _m1, 2);
                    sum_proba_x_cube_diff_energy_m1 += proba * std::pow(energy - _m1, 3);
                }
                _m2 = sum_proba_x_carre_diff_energy_m1 / sum_proba;
                _m3 = sum_proba_x_cube_diff_energy_m1 / sum_proba;
                return std::fabs(sum_proba - 1.) > 1e-6;
            }

            /**
             * @brief ...
             * @return  ...
             */
            std::size_t getChecksum(bool _with_configuration) const
            {
                if (_with_configuration)
                {
                    return m_checksum_all;
                }
                else
                {
                    return m_checksum_data;
                }
            }

            /**
             * @brief ...
             * @param _description:  ...
             */
            void setDescription(const std::string &_description)
            {
                m_description = _description;
            }

            /**
             * @brief ...
             * @return  ...
             */
            const std::string getDescription() const
            {
                return m_description;
            }

            /**
             * @brief ...
             * @param _configuration:  ...
             */
            void setConfiguration(const std::string &_configuration)
            {
                m_configuration = _configuration;
            }

            /**
             * @brief ...
             * @return  ...
             */
            const std::string getConfiguration() const
            {
                return m_configuration;
            }

            /**
             * @brief Get size
             * @param _nbRaies: ...
             * @return ...
             */
            std::size_t getSize(const unsigned int _nbRaies, bool _with_configuration) const
            {
                std::size_t size = sizeof(unsigned int) + sizeof(double) + _nbRaies * Raie::getSize();
                if (_with_configuration)
                {
                    // len m_description + m_description
                    size += sizeof(unsigned int) + m_description.length();
                    // len m_configuration + m_configuration
                    size += sizeof(unsigned int) + m_configuration.length();
                }
                return size;
            }

            /**
             * @brief Get size
             * @return ...
             */
            std::size_t getSize(bool _with_configuration) const
            {
                return getSize(m_raies.size(), _with_configuration);
            }

            /**
             * @brief Get buffer
             * @return ...
             */
            const std::vector<char> serialize(bool _with_configuration) const
            {
                size_t size = getSize(_with_configuration);
                std::vector<char> buffer(size);
                char *ptr = (char *)(buffer.data());

                const unsigned int nbRaies = m_raies.size();
                memcpy(ptr, &nbRaies, sizeof(unsigned int));

                { // check
                    unsigned int tmp;
                    memcpy(&tmp, ptr, sizeof(unsigned int));
                    assert(nbRaies == tmp);
                } // end check

                ptr += sizeof(unsigned int);
                double forcOsc;
                getForcOsc(forcOsc);
                memcpy(ptr, &forcOsc, sizeof(double));

                { // check
                    double tmp;
                    memcpy(&tmp, ptr, sizeof(double));
                    assert(forcOsc == tmp);
                } // end check

                ptr += sizeof(double);
                for (const auto raie : m_raies)
                {
                    const float enerRy = raie.GetEnerRy();
                    memcpy(ptr, &enerRy, sizeof(float));

                    { // check
                        float tmp;
                        memcpy(&tmp, ptr, sizeof(float));
                        assert(enerRy == tmp);
                    } // end check

                    ptr += sizeof(float);
                    const float proba = raie.GetProba();
                    memcpy(ptr, &proba, sizeof(float));

                    { // check
                        float tmp;
                        memcpy(&tmp, ptr, sizeof(float));
                        assert(proba == tmp);
                    } // end check

                    ptr += sizeof(float);
                    const unsigned char J21Ini = raie.GetJ21Ini();
                    memcpy(ptr, &J21Ini, sizeof(unsigned char));

                    { // check
                        unsigned char tmp;
                        memcpy(&tmp, ptr, sizeof(unsigned char));
                        assert(J21Ini == tmp);
                    } // end check

                    ptr += sizeof(unsigned char);
                    const unsigned char J21Fin = raie.GetJ21Fin();
                    memcpy(ptr, &J21Fin, sizeof(unsigned char));

                    { // check
                        unsigned char tmp;
                        memcpy(&tmp, ptr, sizeof(unsigned char));
                        assert(J21Fin == tmp);
                    } // end check

                    ptr += sizeof(unsigned char);
                }

                assert(ptr == buffer.data() + getSize(false));

                { // check all
                    ptr = (char *)(buffer.data());
                    {
                        unsigned int tmp;
                        memcpy(&tmp, ptr, sizeof(unsigned int));
                        assert(nbRaies == tmp);
                    }
                    ptr += sizeof(unsigned int);
                    {
                        double tmp;
                        memcpy(&tmp, ptr, sizeof(double));
                        assert(forcOsc == tmp);
                    }
                    ptr += sizeof(double);
                    for (const auto raie : m_raies)
                    {
                        const float enerRy = raie.GetEnerRy();
                        {
                            float tmp;
                            memcpy(&tmp, ptr, sizeof(float));
                            assert(enerRy == tmp);
                        }
                        ptr += sizeof(float);
                        const float proba = raie.GetProba();
                        {
                            float tmp;
                            memcpy(&tmp, ptr, sizeof(float));
                            assert(proba == tmp);
                        }
                        ptr += sizeof(float);
                        const unsigned char J21Ini = raie.GetJ21Ini();
                        {
                            unsigned char tmp;
                            memcpy(&tmp, ptr, sizeof(unsigned char));
                            assert(J21Ini == tmp);
                        }
                        ptr += sizeof(unsigned char);
                        const unsigned char J21Fin = raie.GetJ21Fin();
                        {
                            unsigned char tmp;
                            memcpy(&tmp, ptr, sizeof(unsigned char));
                            assert(J21Fin == tmp);
                        }
                        ptr += sizeof(unsigned char);
                    }
                    assert(ptr == buffer.data() + getSize(false));
                } // end check all

                if (m_checksum_data)
                {
                    std::vector<char> tmp(getSize(false));
                    auto a = buffer.begin();
                    auto b = tmp.begin();
                    for (; b != tmp.end(); a++, b++)
                    {
                        *b = *a;
                    }
                    if (m_checksum_data != computeChecksum(tmp))
                    {
                        GURU("Record::get invalid checksum: " << m_checksum_data << " != " << computeChecksum(buffer));
                    }
                }

                if (_with_configuration)
                {
                    char *ptr_configuration = ptr;

                    const unsigned int len_description = m_description.length();
                    memcpy(ptr, &len_description, sizeof(unsigned int));

                    { // check
                        unsigned int tmp;
                        memcpy(&tmp, ptr, sizeof(unsigned int));
                        assert(len_description == tmp);
                    } // end check

                    ptr += sizeof(unsigned int);

                    memcpy(ptr, m_description.c_str(), len_description * sizeof(char));

                    { // check
                        std::string tmp;
                        tmp.resize(len_description);
                        memcpy((char *)(tmp.c_str()), ptr, len_description * sizeof(char));
                        assert(m_description == tmp);
                    } // end check

                    ptr += len_description * sizeof(char);

                    const unsigned int len_configuration = m_configuration.length();
                    memcpy(ptr, &len_configuration, sizeof(unsigned int));

                    { // check
                        unsigned int tmp;
                        memcpy(&tmp, ptr, sizeof(unsigned int));
                        assert(len_configuration == tmp);
                    } // end check

                    ptr += sizeof(unsigned int);

                    memcpy(ptr, m_configuration.c_str(), len_configuration * sizeof(char));

                    { // check
                        std::string tmp;
                        tmp.resize(len_configuration);
                        memcpy((char *)(tmp.c_str()), ptr, len_configuration * sizeof(char));
                        assert(m_configuration == tmp);
                    } // end check

                    ptr += len_configuration * sizeof(char);

                    assert(ptr == buffer.data() + buffer.size());

                    { // check all
                        ptr = ptr_configuration;
                        {
                            unsigned int tmp;
                            memcpy(&tmp, ptr, sizeof(unsigned int));
                            assert(len_description == tmp);
                        }
                        ptr += sizeof(unsigned int);

                        {
                            std::string tmp;
                            tmp.resize(len_description);
                            memcpy((char *)(tmp.c_str()), ptr, len_description * sizeof(char));
                            assert(m_description == tmp);
                        } // end check

                        ptr += len_description * sizeof(char);

                        {
                            unsigned int tmp;
                            memcpy(&tmp, ptr, sizeof(unsigned int));
                            assert(len_configuration == tmp);
                        }

                        ptr += sizeof(unsigned int);

                        {
                            std::string tmp;
                            tmp.resize(len_configuration);
                            memcpy((char *)(tmp.c_str()), ptr, len_configuration * sizeof(char));
                            assert(m_configuration == tmp);
                        }

                        ptr += len_configuration * sizeof(char);

                        assert(ptr == buffer.data() + buffer.size());
                    } // check_all

                    if (m_checksum_all && m_checksum_all != computeChecksum(buffer))
                    {
                        GURU("Record::get invalid checksum: " << m_checksum_all << " != " << computeChecksum(buffer));
                    }

                    { // check all
                        ptr = (char *)(buffer.data());
                        {
                            unsigned int tmp;
                            memcpy(&tmp, ptr, sizeof(unsigned int));
                            assert(nbRaies == tmp);
                        }
                        ptr += sizeof(unsigned int);
                        {
                            double tmp;
                            memcpy(&tmp, ptr, sizeof(double));
                            assert(forcOsc == tmp);
                        }
                        ptr += sizeof(double);
                        for (const auto raie : m_raies)
                        {
                            const float enerRy = raie.GetEnerRy();
                            {
                                float tmp;
                                memcpy(&tmp, ptr, sizeof(float));
                                assert(enerRy == tmp);
                            }
                            ptr += sizeof(float);
                            const float proba = raie.GetProba();
                            {
                                float tmp;
                                memcpy(&tmp, ptr, sizeof(float));
                                assert(proba == tmp);
                            }
                            ptr += sizeof(float);
                            const unsigned char J21Ini = raie.GetJ21Ini();
                            {
                                unsigned char tmp;
                                memcpy(&tmp, ptr, sizeof(unsigned char));
                                assert(J21Ini == tmp);
                            }
                            ptr += sizeof(unsigned char);
                            const unsigned char J21Fin = raie.GetJ21Fin();
                            {
                                unsigned char tmp;
                                memcpy(&tmp, ptr, sizeof(unsigned char));
                                assert(J21Fin == tmp);
                            }
                            ptr += sizeof(unsigned char);
                        }
                        assert(ptr == buffer.data() + getSize(false));

                        {
                            unsigned int tmp;
                            memcpy(&tmp, ptr, sizeof(unsigned int));
                            assert(len_description == tmp);
                        }
                        ptr += sizeof(unsigned int);

                        {
                            std::string tmp;
                            tmp.resize(len_description);
                            memcpy((char *)(tmp.c_str()), ptr, len_description * sizeof(char));
                            assert(m_description == tmp);
                        } // end check

                        ptr += len_description * sizeof(char);

                        {
                            unsigned int tmp;
                            memcpy(&tmp, ptr, sizeof(unsigned int));
                            assert(len_configuration == tmp);
                        }

                        ptr += sizeof(unsigned int);

                        {
                            std::string tmp;
                            tmp.resize(len_configuration);
                            memcpy((char *)(tmp.c_str()), ptr, len_configuration * sizeof(char));
                            assert(m_configuration == tmp);
                        }

                        ptr += len_configuration * sizeof(char);

                        assert(ptr == buffer.data() + buffer.size());
                    } // check_all
                }

                return buffer;
            }

            /**
             * @brief Set buffer
             * @return ...
             */
            void deserialize(const std::vector<char> &_buffer)
            {
                clear();
                char *ptr = (char *)(_buffer.data());
                unsigned int nbRaies;
                memcpy(&nbRaies, ptr, sizeof(unsigned int));
                reserveRaies(nbRaies);
                ptr += sizeof(unsigned int);
                double forcOsc;
                memcpy(&forcOsc, ptr, sizeof(double));
                ptr += sizeof(double);
                setForcOsc(forcOsc);
                for (unsigned int iRaie = 0; iRaie < nbRaies; ++iRaie)
                {
                    float enerRy;
                    memcpy(&enerRy, ptr, sizeof(float));
                    ptr += sizeof(float);
                    float proba;
                    memcpy(&proba, ptr, sizeof(float));
                    ptr += sizeof(float);
                    unsigned char J21Ini;
                    memcpy(&J21Ini, ptr, sizeof(unsigned char));
                    ptr += sizeof(unsigned char);
                    unsigned char J21Fin;
                    memcpy(&J21Fin, ptr, sizeof(unsigned char));
                    ptr += sizeof(unsigned char);
                    appendRaie(enerRy, proba, J21Ini, J21Fin);
                }
                assert(nbRaies == m_raies.size());

                setChecksum(_buffer, false);
            }

            /**
             * @brief Dump
             * @return ...
             */
            void dump() const
            {
                std::cout << "dump forcOsc: " << m_forcOsc << std::endl;
                std::cout << "dump nbRaies: " << m_raies.size() << std::endl;
                unsigned int iRaie = 0;
                for (const auto &raie : m_raies)
                {
                    std::cout << "dump #" << iRaie++ << " "
                              << raie.GetEnerRy() << " "
                              << raie.GetProba() << " "
                              << (int)(raie.GetJ21Ini()) << " "
                              << (int)(raie.GetJ21Fin()) << std::endl;
                }
                std::cout << "dump data checksum: " << m_checksum_data << std::endl;
                std::cout << "dump description:" << std::endl;
                std::cout << m_description << std::endl;
                std::cout << "dump configuration:" << std::endl;
                std::cout << m_configuration << std::endl;
                std::cout << "dump all checksum: " << m_checksum_all << std::endl;
            }

            /**
             * @brief ...
             * @param _buffer:  ...
             */
            std::size_t computeChecksum(const std::vector<char> &_buffer) const
            {
                std::size_t checksum = _buffer.size();
                for (const auto &character : _buffer)
                {
                    checksum ^= (std::size_t)character + 0x9e3779b9 + (checksum << 6) + (checksum >> 2);
                }
                return checksum;
            }

        private:
            double m_forcOsc = 0.;
            std::vector<Raie> m_raies;
            std::size_t m_checksum_data = 0;
            std::size_t m_checksum_all = 0;

            // la description de la configuration (la première ligne du mcdf.don_#)
            std::string m_description;
            // le contenu de la configuration hors la description
            std::string m_configuration;

            /**
             * @brief ...
             * @param _buffer:  ...
             */
            void setChecksum(const std::vector<char> &_buffer, bool _with_configuration)
            {
                if (_with_configuration)
                {
                    m_checksum_all = computeChecksum(_buffer);
                }
                else
                {
                    m_checksum_data = computeChecksum(_buffer);
                }
            }
        };

        // la description de la data base chargée en mémoire
        Record m_record;

        // temporary...
        std::string m_path_read, m_path_write;

        class IndexationHierarchique
        {
            private:

                long m_limit_size_file {(long)1024*1024*1024*500};

                void indexation_hierarchique_environment()
                {
                    const char *env = std::getenv("OBADES_LIMIT_SIZE_FILE");
                    if (env)
                    {
                        m_limit_size_file = std::stol(env);
                    }
                }

                /**
                 * @brief Création des répertoires manquants
                 * @param _filename: nom du fichier incluant des sous-répertoires
                 */
                void create_directories(const std::string &_filename) const
                {
                    std::string filenamem_class_indexation = std::filesystem::current_path().string() + std::string("/") + _filename;
                    std::filesystem::create_directories(_filename.substr(0, _filename.rfind('/')));
                }

            public:
                IndexationHierarchique() {
                    indexation_hierarchique_environment();
                }
                ~IndexationHierarchique() {}

                //----------------------------

            private:

                std::ifstream m_datastream_input;
                std::ofstream m_datastream_output;
            
                bool find(std::fstream &file, const std::string _key, std::size_t &_offset_file) const
                {
                    TRACE("IH::find recherche de la clé \'" << _key << "\'")

                    file.seekg(0, std::fstream::end);
                    std::fstream::pos_type offset_end_file = file.tellg();

                    std::fstream::pos_type offset_min = 0;
                    std::fstream::pos_type offset_max = (offset_end_file / 64);
                    std::fstream::pos_type offset_current = (offset_max - offset_min) >> 1;
                    TRACE("IH::find offset_min = " << offset_min)
                    TRACE("IH::find offset_max = " << offset_max)

                    std::string buffer {"                                                               \n"};

                    while(true)
                    {
                        file.seekg(offset_current * 64);
                        file.read((char*)(buffer.c_str()), 64);

                        std::size_t current = 0;

                        for(; buffer[current] == _key[current] && current < 46; ++current);
                        if(current == 46)
                        {
                            TRACE("IH::find trouvé")
                            break;
                        }

                        if (offset_current == offset_min ||
                            offset_current == offset_max)
                        {
                            TRACE("IH::find non trouvé")
                            return false;
                        }

                        if(buffer[current] < _key[current])
                        {
                            offset_min = offset_current;
                            offset_current = ((offset_max - offset_min) >> 1) + offset_min;
                        } else {
                            offset_max = offset_current;
                            offset_current = ((offset_max - offset_min) >> 1) + offset_min;
                        }
                    }
                    
                    _offset_file = offset_current * 64;

                    TRACE("IH::find trouvé à l'offset = " << offset_current)
                    return true;
                }

                const std::string clean(const std::string &_key) const{
                    std::string key = _key;

                    TRACE("IH::clean before extend " << key << " " << key.size())
                    // 53
                    for (int pos = key.size(); pos < 53; ++pos)
                    {
                        key += '0';
                    }
                    TRACE("IH::clean before erase " << key << " " << key.size())
                    key.erase(std::remove(key.begin(), key.end(), '/'), key.end());
                    key = key.substr(4);
                    TRACE("IH::clean " << key << " " << key.size())
                    return key;
                }

                void create_next_offset(const std::string &_filename) const {
                    std::fstream fichier(_filename, std::ios::app);
                    std::string offset_str = string_format("%3u%14u", 0, 0); // 17c
                    fichier << offset_str;
                    fichier.close();
                    TRACE("IH::create_next_offset " << _filename << " offset = " << offset_str)
                }

            public:

                // Recupere l'offset de la prochaine configuration
                // Cet appel doit etre fait au moins apres avoir enregistré une premiere configuration dans la base,
                // cela peut avoir été fait dans une précédente exécution.
                std::size_t GetOffset(const std::string &_path_write, const std::string &_extension, unsigned int &_data_file_index) const
                {
                    const std::string filename {_path_write + "nextOffset" + _extension + ".obades"};
                    if (!std::filesystem::exists(filename))
                    {
                        TRACE("IH::GetOffset " << filename << " NOT EXIST !")
                        create_next_offset(filename);
                    }
                    TRACE("IH::GetOffset " << filename << " EXIST")
                    std::fstream fichier(filename, std::ios::in);
                    std::string offset_str {"99999999999999999\n"};
                    fichier.seekg(0);
                    fichier.read((char*)(offset_str.c_str()), 17);
                    fichier.close();
                    {
                        std::stringstream sstream(offset_str.substr(0, 3));
                        sstream >> _data_file_index;
                    }
                    std::size_t offset {0};
                    {
                        std::stringstream sstream(offset_str.substr(3));
                        sstream >> offset;
                    }
                    TRACE("IH::GetOffset " << filename << "  data_file_index:" << _data_file_index << " offset:" << offset << " GETOFFSET")
                    return offset;
                }

                // Positionne l'offset de la prochaine configuration
                // Cet appel crée le fichier s'il n'existe pas.
                void SetOffset(const std::string &_path_write, const std::string &_extension, const unsigned int _data_file_index, const std::size_t _offset)
                {
                    const std::string filename {_path_write + "nextOffset" + _extension + ".obades"};
                    if (!std::filesystem::exists(filename))
                    {
                        TRACE("IH::SetOffset " << filename << " NOT EXIST!");
                        create_next_offset(filename);
                    }
                    TRACE("IH::SetOffset " << filename << " EXIST");
                    std::fstream fichier(filename, std::ios::out);
                    std::string offset_str = string_format("%3u%14u\n", _data_file_index, _offset); // 17c
                    fichier.seekg(0);
                    fichier.write(offset_str.c_str(), 18);
                    fichier.close();
                    TRACE("IH::SetOffset " << filename << " data_file_index:" << _data_file_index << " offset:" << _offset << " (" << offset_str << ") SETOFFSET");
                }

                const std::string GetZ(const std::string &_key_name) const
                {
                    // _key_name=##/##[...]
                    assert (_key_name.length() >= 3);
                    assert (_key_name[2] == '/');
                    TRACE("IH::GetZ " + _key_name.substr(0, 2))
                    return _key_name.substr(0, 2);
                }

                const std::string GetNe(const std::string &_key_name) const
                {
                    // _key_name=##/##[...]
                    assert (_key_name.length() >= 5);
                    assert (_key_name[2] == '/');
                    assert (_key_name[5] == '/');
                    TRACE("IH::GetNe " + _key_name.substr(3, 2))
                    return _key_name.substr(3, 2);
                }

                std::ofstream& GetWriteDataFilename(const std::string &_path_write, std::string _extension, unsigned int &_data_file_index)
                {
                    std::string data_filename = _path_write + "binary" + _extension + ".obades" + string_format("%u", _data_file_index);
                    TRACE("IH::GetWriteDataFilename " << data_filename);
                    if (!std::filesystem::exists(data_filename))
                    {
                        TRACE("IH::GetWriteDataFilename " << data_filename << " NOT EXIST");
                        create_directories(data_filename);
                    } else {
                        TRACE("IH::GetWriteDataFilename " << data_filename << " size:" << std::filesystem::file_size(data_filename));
                        if (std::filesystem::file_size(data_filename) > m_limit_size_file) {
                            TRACE("IH::GetWriteDataFilename " << data_filename << " TOO BIG");
                            _data_file_index += 1;
                            SetOffset(_path_write, _extension, _data_file_index, 0);
                            data_filename = _path_write + "binary" + _extension + ".obades" + string_format("%u", _data_file_index);
                            if (std::filesystem::exists(data_filename))
                            {
                                TRACE("IH::GetWriteDataFilename " << data_filename << " MUST NOT EXIST ALREADY!");
                                exit(-1);
                            }
                            create_directories(data_filename);
                        }
                    }
                    m_datastream_output.open(data_filename, std::ios::binary | std::ios::app);
                    TRACE("IH::GetWriteDataFilename " << data_filename);
                    return m_datastream_output;
                }

                std::ifstream& GetReadDataFilename(const std::string &_path_write, const std::string &_extension, const unsigned int _data_file_index)
                {
                    std::string data_filename = _path_write + "binary" + _extension + ".obades" + string_format("%u", _data_file_index);
                    TRACE("IH::GetReadDataFilename " << data_filename);
                    m_datastream_input.open(data_filename, std::ios::in);
                    return m_datastream_input;
                }

                std::size_t GetReferenceOffset(const std::string &_path_write, const std::string &_key_name, unsigned int &_data_file_index) const
                {
                    std::string filename_indexation = _path_write + "/tableIndexNe" + _key_name.substr(3, 2) + ".obades";
                    TRACE("IH::GetReferenceOffset " << filename_indexation);
                    std::size_t offset {0};

                    std::fstream file(filename_indexation, std::ios::in);
                    std::size_t offset_file {0};

                    if(!find(file, clean(_key_name), offset_file))
                    {
                        GURU("IH::GetReferenceOffset " << _key_name << "(" << clean(_key_name) << ") not finded!");
                        throw ("Invalid key name cause not finded!");
                    }

                    std::string buffer {"                 "};

                    file.seekg(offset_file + 46);
                    file.read((char*)(buffer.c_str()), 17);

                    {
                        std::stringstream sstream(buffer.substr(0, 3));
                        sstream >> _data_file_index;
                    }
                    {
                        std::stringstream sstream(buffer.substr(3));
                        sstream >> offset;
                    }

                    file.close();

                    if(_data_file_index == 999)
                    {
                        GURU("IH::GetReferenceOffset " << _key_name << "(" << clean(_key_name) << ") _data_file_index is 999");
                        throw ("Valid key name but invalid _data_file_index!");
                    }
                    if(offset == 99999999999999)
                    {
                        GURU("IH::GetReferenceOffset " << _key_name << "(" << clean(_key_name) << ") offset is 99999999999999!");
                        throw ("Valid key name but invalid offset!");
                    }
                    return offset;
                }

                void SetReferenceOffset(const std::string &_path_read, const std::string &_path_write, const std::string &_key_name, const unsigned int _data_file_index, const std::size_t _offset) const
                {
                    std::string filename_indexation = _path_write + "/tableIndexNe" + _key_name.substr(3, 2) + ".obades";
                    TRACE("IH::SetReferenceOffset " << filename_indexation);
                    create_directories(filename_indexation);
                    if (!std::filesystem::exists(filename_indexation))
                    {
                        std::string filename_indexation_origin = _path_read + "/NE/tableIndexNe" + _key_name.substr(3, 2) + ".obades";
                        try
                        {
                            std::filesystem::copy(filename_indexation_origin, filename_indexation);
                        }
                        catch(...)
                        {
                            GURU("IH::SetReferenceOffset Don't exit " << filename_indexation_origin << " file!");
                            throw (std::string("Don't exit ") + filename_indexation_origin + " file!");
                        }
                    }

                    std::fstream file(filename_indexation, std::ios::in | std::ios::out);
                    std::size_t offset_file {0};

                    if(!find(file, clean(_key_name), offset_file))
                    {
                        GURU("IH::SetReferenceOffset " << _key_name << "(" << clean(_key_name) << ") not finded!");
                        throw ("Invalid key name cause not finded!");
                    }

                    std::string offset_str = string_format("%3u%14u", _data_file_index, _offset); // 17c

                    file.seekg(offset_file + 46);
                    file.write(offset_str.c_str(), 17);
                    file.close();
                }

                void List(const std::string &_filename, const std::string &_z_ne) const {
                    std::fstream file(_filename, std::ios::in);
                    std::size_t offset_file {0};

                    file.seekg(0, std::fstream::end);
                    std::fstream::pos_type offset_end_file = file.tellg();

                    std::fstream::pos_type offset_min = 0;
                    std::fstream::pos_type offset_max = (offset_end_file / 64);

                    std::string buffer {"                                                               \n"};

                    for(std::fstream::pos_type offset_current = offset_min; offset_current < offset_max; offset_current+=1)
                    {
                        file.seekg(offset_current * 64);
                        file.read((char*)(buffer.c_str()), 64);

                        std::size_t offset {0};
                        std::stringstream sstream(buffer.substr(46, 64));
                        sstream >> offset;

                        if(offset != 99999999999999999)
                        {
                            std::cout << "   " << _z_ne << "/" << buffer.substr(0,4) << "/" << buffer.substr(4, 42) << "\n"; 
                        }
                    }

                    file.close();
                }
        };

        // Indexation m_class_indexation;
        IndexationHierarchique m_class_indexation_hierarchique;

        std::string m_ascii_subdirdon {"mcdfDon"};
        bool m_accumulated_binary_by_z_ne {true};

        void obades_environnement()
        {
            const char *env = std::getenv("OBADES_TRACE");
            if (env)
            {
                if (!strcmp(env, "0"))
                {
                    SET_TRACE(false);
                }
                else
                {
                    SET_TRACE(true);
                    TRACE("obades parameters:")
                    TRACE("  path_read = " << m_path_read)
                    TRACE("  path_write = " << m_path_write)
                    TRACE("  trace = " << trace)
                    TRACE("obades environment:")
                    TRACE("  OBADES_TRACE=" << trace)
                }
            } else {
                TRACE("obades parameters:")
                TRACE("  path_read = " << m_path_read)
                TRACE("  path_write = " << m_path_write)
                TRACE("  trace = " << trace)
                TRACE("obades environment:")
                TRACE("  OBADES_TRACE=" << trace)
            }

            env = std::getenv("OBADES_ASCII_SUBDIRDON");
            if (env)
            {
                m_ascii_subdirdon = env;
                TRACE("  OBADES_ASCII_SUBDIRDON=" << m_ascii_subdirdon)
            } else {
                TRACE("  OBADES_ASCII_SUBDIRDON=" << m_ascii_subdirdon << " (default)")
            }

            env = std::getenv("OBADES_ACCUMLATED_BINARY_BY_NE");
            if (env)
            {
                if (!strcmp(env, "0"))
                {
                    m_accumulated_binary_by_z_ne = false;
                    TRACE("  OBADES_ACCUMLATED_BINARY_BY_NE=" << m_accumulated_binary_by_z_ne)
                }
                else
                {
                    m_accumulated_binary_by_z_ne = true;
                    TRACE("  OBADES_ACCUMLATED_BINARY_BY_NE=" << m_accumulated_binary_by_z_ne)
                }
            } else {
                TRACE("  OBADES_ACCUMLATED_BINARY_BY_NE=" << m_accumulated_binary_by_z_ne << " (default)")
            }
        }


    public:
        /**
         * @brief Constructeur
         * Le constructeur exploite la variable d'environnement OBADES_TRACE pour positionner
         * à l'exécution l'activation ou non des traces.
         * @param _path_read (in): ...
         * @param _path_write (in): ...
         * @param _trace (in): ...
         */
        obades(const std::string _path_read,
               const std::string _path_write, const bool _trace=false)
            : m_path_read(_path_read), m_path_write(_path_write)
        {
            SET_TRACE(_trace);
            obades_environnement();

            bool force = false;
            const std::string filename {m_path_write + "/accumulateBinary/nextOffset.obades"};
            TRACE(filename << " exist?");
            TRACE(std::filesystem::exists(filename));
            TRACE(filename << " exist? B");
            if (std::filesystem::exists(filename))
            {
                TRACE("Base WITH accumulated binary by Z NE");
                m_accumulated_binary_by_z_ne = false;
                TRACE("FORCE OBADES_ACCUMLATED_BINARY_BY_NE=" << m_accumulated_binary_by_z_ne << " (false), cause " << filename << " exist!");
                force = true;
            } else {
                TRACE("Base WITHOUT accumulated binary by Z NE");
                for (unsigned int Z = 0; Z < 100; ++Z)
                {
                    const std::string directory = m_path_write + "/accumulateBinary/" + string_format("%02u", Z);
                    if (std::filesystem::exists(directory))
                    {
                        m_accumulated_binary_by_z_ne = true;
                        TRACE("FORCE OBADES_ACCUMLATED_BINARY_BY_NE=" << m_accumulated_binary_by_z_ne << " (true), cause directory (" << directory << ") exist!");
                        force = true;
                        break;
                    } 
                }
            }
            if (!force)
            {
                TRACE("NO FORCE OBADES_ACCUMLATED_BINARY_BY_NE (" << m_accumulated_binary_by_z_ne << ") cause create database!");
            }
        }

        /**
         * @brief Destructeur
         */
        ~obades() {}

        /**
         * @brief Lecture d'un enregistrement ASCII
         * Dans un premier temps, on tente de lire ce fichier dans m_path_read
         * sinon dans m_path_write.
         * @param _filename: ...
         * @param _checksum_data: checksum data file
         * @param _checksum_all: checksum data and mcdf.don files
         */
        int readAscii(const std::string _filename, const std::size_t _checksum_data = 0, const std::size_t _checksum_all = 0)
        {
            m_key_name = "";
            // clear structure
            TRACE("readAscii clear")
            m_record.clear();
            // data file exists
            TRACE("readAscii filename: " << _filename)
            std::string filename = m_path_read + "/" + _filename;
            std::string line;
            if (!std::filesystem::exists(filename))
            {
                TRACE("readAscii data file (" << filename << ") not exist!")
                filename = m_path_write + "/ascii/" + _filename;
                if (!std::filesystem::exists(filename))
                {
                    GURU("readAscii data file (" << filename << ") not exist!")
                    RETURN("readAscii", ERROR_DB_ASCII_FILE_NOT_EXIST)
                }
            } else {
                TRACE("readAscii data file (" << filename << ") exist")
            }
            // mcdf.don exists
            std::string filename_don = filename;
            std::string::size_type pos = filename_don.rfind('/');
            // OLD std::string m_ascii_subdirdon = m_obades_configuration["mcdfDb"]["ascii"]["subdirdon"];
            if (m_ascii_subdirdon.empty())
            {
                filename_don.insert(pos + 1, "mcdf.don_");
            } else {
                filename_don.insert(pos + 1, m_ascii_subdirdon + "/mcdf.don_");
                std::string::size_type npos = pos + 1 + m_ascii_subdirdon.length() + 10;
                // path data .../ 2# (Z] / 2# (NE) / 4# (transition) / name... 
                if (filename_don[pos-11] != '/' || filename_don[pos-8] != '/')
                {
                    GURU("readAscii data file (" << filename << " in " << pos - 11 << " is " << filename_don[pos-11] << " or "
                                                                       << pos -  8 << " is " << filename_don[pos-8] << ") not correct for Z.../ 2# (Z) / 2# (ne) / 4# (transition) /... !")
                    RETURN("readAscii", ERROR_DB_ASCII_FILE_NOT_EXIST)
                }
                if (filename_don[pos-5] != '/')
                {
                    GURU("readAscii data file (" << filename << " in " << pos - 5 << " is " << filename_don[pos-5] << ") not correct for NE.../ 2# (Z) / 2# (ne) / 4# (transition) /... !")
                    RETURN("readAscii", ERROR_DB_ASCII_FILE_NOT_EXIST)
                }
                filename_don.insert(npos, "Met1_" + filename_don.substr(pos-10, 2) + "_" + filename_don.substr(pos-7, 2) + "_" + filename_don.substr(pos-4, 4) + "_");
                std::ostringstream oss;
                for (int iMet = 0; iMet<=9 && !std::filesystem::exists(filename_don); ++iMet)
                {
                    oss.clear(); oss.str("");
                    oss << iMet;
                    filename_don.replace(npos + 3, 1, oss.str());
                }
            }
            if (!std::filesystem::exists(filename_don))
            {
                GURU("readAscii mcdf.don file (" << filename_don << ") not exist!")
                filename_don = "";
            } else {
                TRACE("readAscii data file (" << filename_don << ") exist")
            }
            // read data file
            std::fstream text(filename);
            if (!std::getline(text, line))
            {
                GURU("readAscii data file (" << filename << ") empty!")
                RETURN("readAscii", ERROR_PARAMETERS_FILE_NOT_EXIST)
            }
            // read and decode data file
            unsigned int nbRaies = std::atoi(line.substr(0, 12).c_str());
            TRACE("  nbRaies = " << nbRaies)
            m_record.reserveRaies(nbRaies);
            double forcOsc = std::atof(line.substr(13).c_str());
            TRACE("  forcOsc = " << forcOsc)
            m_record.setForcOsc(forcOsc);
            unsigned int iLine = 0;
            while (std::getline(text, line))
            {
                iLine++;
                float enerRy = std::atof(line.substr(0, 12).c_str());
                float proba = std::atof(line.substr(13, 25).c_str());
                unsigned int J21Ini = std::atoi(line.substr(25, 29).c_str());
                unsigned int J21Fin = std::atoi(line.substr(29).c_str());
                if (J21Fin == 0)
                {
                    J21Fin = std::numeric_limits<unsigned char>::max();
                }
                TRACE("  #" << iLine << " " << enerRy << " " << proba << " " << J21Ini << " " << J21Fin)
                m_record.appendRaie(enerRy, proba, J21Ini, J21Fin);
            }
            text.close();
            // checked some aspect
            if (iLine != m_record.getNumberOfRaies())
            {
                GURU("readAscii (" << filename << ") number of lines not corresponding in record!")
                RETURN("readAscii", ERROR_DB_ASCII_FORMAT)
            }
            if (iLine != nbRaies)
            {
                GURU("readAscii (" << filename << ") number of lines not corresponding!")
                RETURN("readAscii", ERROR_DB_ASCII_FORMAT)
            }
            // read and decode mcdf.don file
            if (filename_don != "")
            {
                std::string description = "";
                text.open(filename_don);
                if (!std::getline(text, description))
                {
                    GURU("readAscii mcdf.don file (" << filename_don << ") empty!")
                    RETURN("readAscii", ERROR_PARAMETERS_FILE_EMPTY)
                }
                TRACE("  description = " << description << "EOF")
                m_record.setDescription(description);
                std::string configuration = "";
                while (std::getline(text, line))
                {
                    if (configuration == "")
                    {
                        configuration += line;
                    }
                    else
                    {
                        configuration += "\n" + line;
                    }
                }
                TRACE("  configuration = " << configuration << "EOF")
                m_record.setConfiguration(configuration);
            }
            // m_record.dump();
            // checked by external value cheksum
            if (_checksum_data)
            {
                TRACE("readAscii compute data checksum")
                const std::size_t checksum_data = m_record.computeChecksum(m_record.serialize(false));
                if (_checksum_data != checksum_data)
                {
                    GURU("readAscii invalid data checksum: " << _checksum_data << " != " << checksum_data << " (computed)")
                    RETURN("readAscii", ERROR_INVALID_CHECKSUM)
                }
                TRACE("readAscii valid data checksum : " << _checksum_data)
                if (!_checksum_all)
                {
                    const std::size_t checksum_all = m_record.computeChecksum(m_record.serialize(true));
                    TRACE("readAscii all checksum: " << checksum_all)
                }
            }
            if (_checksum_all)
            {
                TRACE("readAscii compute all checksum")
                const std::size_t checksum_all = m_record.computeChecksum(m_record.serialize(true));
                if (_checksum_all != checksum_all)
                {
                    GURU("readAscii invalid all checksum: " << _checksum_all << " != " << checksum_all << " (computed)")
                    RETURN("readAscii", ERROR_INVALID_CHECKSUM)
                }
                TRACE("readAscii valid all checksum: " << _checksum_all)
            }
            RETURN("readAscii", NO_ERROR)
        }

    private:
        void create_directories(const std::string &_filename) const
        {
            TRACE("current path: " << std::filesystem::current_path())
            std::string filename = std::filesystem::current_path().string() + std::string("/") + _filename;
            TRACE("filename: " << filename)
            TRACE("create directories: " << filename.substr(0, filename.rfind('/')))
            std::filesystem::create_directories(filename.substr(0, filename.rfind('/')));
        }

    public:
        /**
         * @brief Ecriture d'un enregistrement ASCII
         * On ecrit cet enregistrement dans m_path_write.
         * @param _filename: ...
         * @param _checksum_data: checksum data file
         * @param _checksum_all: checksum data and mcdf.don files
         */
        int writeAscii(const std::string &_filename, const std::size_t _checksum_data = 0, const std::size_t _checksum_all = 0) const
        {
            TRACE("writeAscii " << _filename)
            std::string filename = m_path_write + "/ascii/" + _filename;
            TRACE("writeAscii " << filename)
            if (std::filesystem::exists(filename))
            {
                GURU("writeAscii data file (" << filename << ") already exist!")
                RETURN("writeAscii", ERROR_DB_ASCII_FILE_ALREADY_EXIST)
            }
            // build repertories
            create_directories(filename);
            // write data file
            std::ofstream fichier(filename, std::ios::out);
            const size_t number = m_record.getNumberOfRaies();
            double forcOsc;
            m_record.getForcOsc(forcOsc);
            TRACE("writeAscii " << number << " " << forcOsc)
            std::string line = string_format("%12d  %1.15f", number, forcOsc);
            TRACE("writeAscii   line: " << line)
            fichier << line << std::endl;

            bool first = true;
            float enerRy;
            float proba;
            unsigned char J21Ini, J21Fin;
            for (size_t index = 0; index < number; ++index)
            {
                if (first)
                {
                    first = false;
                }
                else
                {
                    fichier << std::endl;
                }
                int ierr = m_record.getRaie(index, enerRy, proba, J21Ini, J21Fin);
                assert(ierr == 0);
                if (J21Fin == std::numeric_limits<unsigned char>::max())
                {
                    line = string_format("%1.6E %1.6E %3d", enerRy, proba, J21Ini);
                }
                else
                {
                    line = string_format("%1.6E %1.6E %3d %3d", enerRy, proba, J21Ini, J21Fin);
                }
                fichier << line;
            }
            fichier << std::endl;
            fichier.close();

            // write mcdf.don_#
            std::string filename_don = filename;
            std::string::size_type pos = filename_don.rfind('/');
            // OLD std::string m_ascii_subdirdon = m_obades_configuration["mcdfDb"]["ascii"]["subdirdon"];
            if (m_ascii_subdirdon.empty())
            {
                filename_don.insert(pos + 1, "mcdf.don_");
            } else {
                filename_don.insert(pos + 1, m_ascii_subdirdon + "/mcdf.don_");
                std::string::size_type npos = pos + 1 + m_ascii_subdirdon.length() + 10;
                // path data .../ 2# (Z] / 2# (NE) / 4# (transition) / name... 
                if (filename_don[pos-11] != '/' || filename_don[pos-8] != '/')
                {
                    GURU("readAscii data file (" << filename << " in " << pos - 11 << " is " << filename_don[pos-11] << " or "
                                                                       << pos -  8 << " is " << filename_don[pos-8] << ") not correct for Z.../ 2# (Z) / 2# (ne) / 4# (transition) /... !")
                    RETURN("readAscii", ERROR_DB_ASCII_FILE_NOT_EXIST)
                }
                if (filename_don[pos-5] != '/')
                {
                    GURU("readAscii data file (" << filename << " in " << pos - 5 << " is " << filename_don[pos-5] << ") not correct for NE.../ 2# (Z) / 2# (ne) / 4# (transition) /... !")
                    RETURN("readAscii", ERROR_DB_ASCII_FILE_NOT_EXIST)
                }
                filename_don.insert(npos, "Met9_" + filename_don.substr(pos-10, 2) + "_" + filename_don.substr(pos-7, 2) + "_" + filename_don.substr(pos-4, 4) + "_");
                create_directories(filename_don);
            }
            TRACE("write " << filename_don)
            fichier.open(filename_don);
            fichier << m_record.getDescription() << std::endl;
            fichier << m_record.getConfiguration() << std::endl;
            fichier.close();

            // checked by external value cheksum
            if (_checksum_data)
            {
                std::size_t checksum_data = m_record.getChecksum(false);
                if (!checksum_data)
                {
                    TRACE("writeAscii compute data checksum")
                    checksum_data = m_record.computeChecksum(m_record.serialize(false));
                }
                if (_checksum_data != checksum_data)
                {
                    GURU("writeAscii invalid data checksum: " << _checksum_data << " != " << checksum_data)
                    RETURN("writeAscii", ERROR_INVALID_CHECKSUM)
                }
                TRACE("writeAscii valid data checksum : " << _checksum_data)
                if (!_checksum_all)
                {
                    const std::size_t checksum_all = m_record.computeChecksum(m_record.serialize(true));
                    TRACE("writeAscii all checksum: " << checksum_all)
                }
            }
            if (_checksum_all)
            {
                std::size_t checksum_all = m_record.getChecksum(true);
                if (!checksum_all)
                {
                    TRACE("writeAscii compute all checksum")
                    checksum_all = m_record.computeChecksum(m_record.serialize(true));
                }
                if (_checksum_all != checksum_all)
                {
                    GURU("writeAscii invalid all checksum: " << _checksum_all << " != " << checksum_all)
                    RETURN("writeAscii", ERROR_INVALID_CHECKSUM)
                }
                TRACE("writeAscii valid all checksum: " << _checksum_all)
            }

            RETURN("writeAscii", NO_ERROR)
        }

        /**
         * @brief Ecriture d'un enregistrement ASCII
         * On ecrit cet enregistrement dans m_path_write.
         * @param _pathname: ...
         * @param _filename: ...
         * @param _checksum_data: checksum data file
         * @param _checksum_all: checksum data and mcdf.don files
         */
        int writePathAscii(const std::string &_pathname, const std::string &_filename,
                           const std::size_t _checksum_data = 0, const std::size_t _checksum_all = 0)
        {
            std::string memo_path_write = m_path_write;
            m_path_write = _pathname;
            int ierr = writeAscii(_filename, _checksum_data, _checksum_all);
            if (ierr)
            {
                RETURN("writePathAscii", ierr)
            }
            m_path_write = memo_path_write;
            RETURN("writePathAscii", NO_ERROR)
        }

        /**
         * @brief Lecture d'un enregistrement BINAIRE
         * Dans un premier temps, on tente de lire ce fichier dans m_path_read/binary
         * sinon dans m_path_write/binary.
         * @param _filename: ...
         * @param _checksum_data: checksum data file
         * @param _checksum_all: checksum data and mcdf.don files
         */
        int readBinary(const std::string _filename,
                       const std::size_t _checksum_data = 0, const std::size_t _checksum_all = 0)
        {
            m_key_name = "";
            m_record.clear();

            TRACE("readBinary filename: " << _filename)
            std::string filename = m_path_read + "/" + _filename;

            if (!std::filesystem::exists(filename))
            {
                TRACE("readBinary filename (" << filename << ") not exist!")

                filename = m_path_write + "/binary/" + _filename;
                if (!std::filesystem::exists(filename))
                {
                    GURU("readBinary filename (" << filename << ") not exist!")
                    RETURN("readBinary", ERROR_DB_ASCII_FILE_NOT_EXIST)
                }
            }

            std::ifstream fichier(filename, std::ios::binary);

            // read data
            std::vector<char> buffer(sizeof(unsigned int));
            fichier.seekg(0);
            fichier.read(buffer.data(), buffer.size());
            unsigned int nbRaies;
            memcpy(&nbRaies, buffer.data(), buffer.size());
            TRACE("readBinary nbRaies: " << nbRaies)

            buffer.resize(m_record.getSize(nbRaies, false));
            TRACE("readBinary size:" << buffer.size())

            fichier.seekg(0);
            fichier.read(buffer.data(), buffer.size());
            m_record.deserialize(buffer);

            if (_checksum_data && _checksum_data != m_record.getChecksum(false))
            {
                GURU("readBinary invalid data checksum: " << _checksum_data << " != " << m_record.getChecksum(false))
                RETURN("readBinary", ERROR_INVALID_CHECKSUM)
            }

            // m_record.dump();

            // read mcdf.don_#
            std::size_t pos = m_record.getSize(nbRaies, false);
            std::size_t crt_size = sizeof(unsigned int);
            std::size_t size = pos + crt_size;

            buffer.resize(size);
            fichier.seekg(0);
            fichier.read(buffer.data(), buffer.size());
            unsigned int len_description;
            memcpy(&len_description, buffer.data() + pos, crt_size);
            // TRACE("readBinary len_description: " << len_description)
            pos += crt_size;

            crt_size = len_description * sizeof(char);
            size += crt_size;
            buffer.resize(size);
            fichier.seekg(0);
            fichier.read(buffer.data(), buffer.size());
            std::string description;
            description.resize(len_description);
            char *ptr = buffer.data() + pos;
            char *end = buffer.data() + buffer.size();
            for (unsigned int ind = 0; ptr != end; ++ptr, ++ind)
            {
                ((char *)(description.c_str()))[ind] = *ptr;
            }
            // TRACE("readBinary description: " << description)
            pos += crt_size;

            m_record.setDescription(description);

            crt_size = sizeof(unsigned int);
            size += crt_size;
            buffer.resize(size);
            fichier.seekg(0);
            fichier.read(buffer.data(), buffer.size());
            unsigned int len_configuration;
            memcpy(&len_configuration, buffer.data() + pos, crt_size);
            // TRACE("readBinary len_configuration: " << len_configuration)
            pos += crt_size;

            crt_size = len_configuration * sizeof(char);
            size += crt_size;
            buffer.resize(size);
            fichier.seekg(0);
            fichier.read(buffer.data(), buffer.size());
            std::string configuration;
            configuration.resize(len_configuration);
            ptr = buffer.data() + pos;
            end = buffer.data() + buffer.size();
            for (unsigned int ind = 0; ptr != end; ++ptr, ++ind)
            {
                ((char *)(configuration.c_str()))[ind] = *ptr;
            }
            // TRACE("readBinary configuration: " << configuration)
            pos += crt_size;

            m_record.setConfiguration(configuration);

            fichier.close();

            RETURN("readBinary", NO_ERROR)
        }

        /**
         * @brief Ecriture d'un enregistrement BINAIRE
         * On ecrit cet enregistrement dans m_path_write/binary.
         * @param _filename: ...
         * @param _checksum_data: checksum data file
         * @param _checksum_all: checksum data and mcdf.don files
         */
        int writeBinary(const std::string _filename,
                        const std::size_t _checksum_data = 0, const std::size_t _checksum_all = 0)
        {
            TRACE("writeBinary " << _filename)
            std::string filename = m_path_write + "/binary/" + _filename;
            TRACE("writeBinary " << filename)
            // build repertories
            create_directories(filename);
            // write
            std::ofstream fichier(filename, std::ios::binary);
            fichier.seekp(0);
            const std::vector<char> buffer = m_record.serialize(true);
            TRACE("writeBinary size:" << buffer.size())
            fichier.write(buffer.data(), buffer.size());
            if (_checksum_data)
            {
                std::size_t checksum_data = m_record.getChecksum(false);
                if (!checksum_data)
                {
                    TRACE("writeBinary computeChecksum")
                    checksum_data = m_record.computeChecksum(m_record.serialize(false));
                }
                if (_checksum_data != checksum_data)
                {
                    GURU("writeBinary invalid data checksum: " << _checksum_data << " != " << checksum_data)
                    RETURN("writeBinary", ERROR_INVALID_CHECKSUM)
                }
                TRACE("writeBinary valid data checksum: " << _checksum_data)
                if (!_checksum_all)
                {
                    std::size_t checksum_all = m_record.getChecksum(true);
                    if (!checksum_all)
                    {
                        TRACE("writeBinary computeChecksum")
                        checksum_all = m_record.computeChecksum(buffer);
                    }
                    TRACE("writeBinary all checksum: " << _checksum_all)
                }
            }
            if (_checksum_all)
            {
                std::size_t checksum_all = m_record.getChecksum(true);
                if (!checksum_all)
                {
                    TRACE("writeBinary computeChecksum")
                    checksum_all = m_record.computeChecksum(buffer);
                }
                if (_checksum_all != checksum_all)
                {
                    GURU("writeBinary invalid all checksum: " << _checksum_all << " != " << checksum_all)
                    RETURN("writeBinary", ERROR_INVALID_CHECKSUM)
                }
                TRACE("writeBinary valid all checksum: " << _checksum_all)
            }
            fichier.close();
            RETURN("writeBinary", NO_ERROR)
        }

        /**
         * @brief ...
         * ...
         * @param _input_offset (in): ...
         * @param _file (in/out): ...
         * @param _checksum_data (in): checksum data file
         * @param _checksum_all (in): checksum data and mcdf.don files
         * @param _output_offset (out): ...
         */
        template<typename T>
        int writerRecordBinary(const std::size_t _input_offset,
                               T &_file,
                               std::size_t &_output_offset, const std::size_t _checksum_data, const std::size_t _checksum_all)
            const
        {
            TRACE("writerRecordBinary")
            const std::vector<char> buffer = m_record.serialize(true);
            TRACE("writerRecordBinary size:" << buffer.size() << " _input_offset " << _input_offset)
            _file.seekp(_input_offset);
            TRACE("writerRecordBinary offset before write:" << _file.tellp())
            _file.write(buffer.data(), buffer.size());
            TRACE("writerRecordBinary offset after write:" << _file.tellp())
            if (_checksum_data)
            {
                std::size_t checksum_data = m_record.getChecksum(false);
                if (!checksum_data)
                {
                    TRACE("writerRecordBinary computeChecksum")
                    checksum_data = m_record.computeChecksum(m_record.serialize(false));
                }
                if (_checksum_data != checksum_data)
                {
                    GURU("writerRecordBinary invalid checksum: " << _checksum_data << " != " << checksum_data)
                    RETURN("writerRecordBinary", ERROR_INVALID_CHECKSUM)
                }
                TRACE("writerRecordBinary valid data checksum: " << _checksum_data)
                if (!_checksum_all)
                {
                    std::size_t checksum_all = m_record.getChecksum(true);
                    if (!checksum_all)
                    {
                        TRACE("writerRecordBinary computeChecksum")
                        checksum_all = m_record.computeChecksum(buffer);
                    }
                    TRACE("writerRecordBinary all checksum: " << checksum_all)
                }
            }
            if (_checksum_all)
            {
                std::size_t checksum_all = m_record.getChecksum(true);
                if (!checksum_all)
                {
                    TRACE("writerRecordBinary computeChecksum")
                    checksum_all = m_record.computeChecksum(buffer);
                }
                if (_checksum_all != checksum_all)
                {
                    GURU("writerRecordBinary invalid all checksum: " << _checksum_all << " != " << checksum_all)
                    RETURN("writerRecordBinary", ERROR_INVALID_CHECKSUM)
                }
                TRACE("writerRecordBinary valid all checksum: " << _checksum_all)
            }
            _output_offset = _file.tellp();
            TRACE("writerRecordBinary output_offset: " << _output_offset)
            RETURN("writerRecordBinary", NO_ERROR)
        }

        /**
         * @brief ...
         * ...
         * @param _key_name: is a data key 01/01/0123/01234...
         * @param _checksum_data: checksum data file
         * @param _checksum_all: checksum data and mcdf.don files
         */
        int accumulateRecordBinary(const std::string _key_name,
                                   const std::size_t _checksum_data = 0,
                                   const std::size_t _checksum_all = 0)
        {
            TRACE("accumulateRecordBinary " << _key_name)
            TRACE("  m_path_write = " << m_path_write)
            
            std::string add_path_write = "/accumulateBinary/";
            std::string extension = "";
            if (m_accumulated_binary_by_z_ne)
            {
                TRACE("  m_accumulated_binary_by_z_ne = " << m_accumulated_binary_by_z_ne)
                add_path_write += m_class_indexation_hierarchique.GetZ(_key_name) + "/"; 
                extension = "Ne" + m_class_indexation_hierarchique.GetNe(_key_name);
            }
            TRACE("  add_path_write = " << add_path_write)
            TRACE("  extension = " << extension)
            std::string path_write = m_path_write + add_path_write;
            TRACE("  path_write = " << path_write)
            create_directories(path_write);
            std::string path_write_reference_offset = m_path_write + "/accumulateBinary/" + m_class_indexation_hierarchique.GetZ(_key_name) + "/";
            TRACE("  path_write_reference_offset = " << path_write_reference_offset)
            create_directories(path_write_reference_offset);

            unsigned int data_file_index;
            // Offset global
            std::size_t input_offset = m_class_indexation_hierarchique.GetOffset(path_write, extension, data_file_index);
            TRACE("accumulateRecordBinary input_offset = " << input_offset);

            std::ofstream &fichier = m_class_indexation_hierarchique.GetWriteDataFilename(path_write, extension, data_file_index);
            if(!fichier.is_open())
            {
                ERROR("fichier pas ouvert " << path_write << _key_name)
                exit(-1);
            }
            // Au cas ou le GetWriteDataFilename cree un nouveau fichier
            // Offset global
            input_offset = m_class_indexation_hierarchique.GetOffset(path_write, extension, data_file_index);
            std::size_t output_offset = 0;
            int ierr = writerRecordBinary(input_offset, fichier, output_offset, _checksum_data, _checksum_all);
            if (ierr)
            {
                RETURN("accumulateRecordBinary", ierr);
            }
            TRACE("accumulateRecordBinary output_offset = " << output_offset);
            m_class_indexation_hierarchique.SetOffset(path_write, extension, data_file_index, output_offset);
            m_class_indexation_hierarchique.SetReferenceOffset(m_path_read, path_write_reference_offset, _key_name, data_file_index, input_offset);
            fichier.flush();
            fichier.close();
            RETURN("accumulateRecordBinary", NO_ERROR);
        }

        /**
         * @brief ...
         * ...
         * @param _offset: ...
         * @param _file: ...
         * @param _data_b: charge juste les donnees
         * @param _description_b: charge la description, positionne _data_b=true
         * @param _configuration_b: charge la configuration, positionne _data_b=true et _description_b=true
         * @param _checksum_data: checksum data file
         * @param _checksum_all: checksum data and mcdf.don files
         */
        template<typename T>
        int readerRecordBinary(const std::size_t _offset, T &_file,
                               const bool _data_b, const bool _description_b, const bool _configuration_b,
                               const std::size_t _checksum_data = 0, const std::size_t _checksum_all = 0)
        {
            m_record.clear();

            TRACE("readerRecordBinary")

            // cas trivial et idiot
            if (!_data_b && !_description_b && !_configuration_b)
            {
                RETURN("readerRecordBinary", NO_ERROR)
            }

            std::vector<char> buffer(sizeof(unsigned int));
            _file.seekg(_offset);
            TRACE("readerRecordBinary offset before read: " << _file.tellg())
            _file.read(buffer.data(), buffer.size());
            TRACE("readerRecordBinary offset after read: " << _file.tellg())
            unsigned int nbRaies;
            memcpy(&nbRaies, buffer.data(), buffer.size());
            TRACE("readerRecordBinary nbRaies: " << nbRaies)

            buffer.resize(m_record.getSize(nbRaies, false));
            // TRACE("readerRecordBinary size:" << buffer.size())
            _file.seekg(_offset);
            // TRACE("readerRecordBinary offset before read: " << _file.tellg())
            _file.read(buffer.data(), buffer.size());
            // TRACE("readerRecordBinary offset after read: " << _file.tellg())
            m_record.deserialize(buffer);
            // m_record.dump();

            if (_checksum_data && _checksum_data != m_record.getChecksum(false))
            {
                GURU("readerRecordBinary invalid data checksum: " << _checksum_data << " != " << m_record.getChecksum(false))
                RETURN("readerRecordBinary", ERROR_INVALID_CHECKSUM)
            }

            // m_record.dump();

            if (!_description_b && !_configuration_b)
            {
                RETURN("readerRecordBinary", NO_ERROR)
            }

            // read mcdf.don_#
            std::size_t pos = m_record.getSize(nbRaies, false);
            std::size_t crt_size = sizeof(unsigned int);
            std::size_t size = pos + crt_size;

            buffer.resize(size);
            _file.seekg(_offset);
            _file.read(buffer.data(), buffer.size());
            unsigned int len_description;
            memcpy(&len_description, buffer.data() + pos, crt_size);
            // TRACE("readBinary len_description: " << len_description)
            pos += crt_size;

            crt_size = len_description * sizeof(char);
            size += crt_size;
            buffer.resize(size);
            _file.seekg(_offset);
            _file.read(buffer.data(), buffer.size());
            std::string description;
            description.resize(len_description);
            char *ptr = buffer.data() + pos;
            char *end = buffer.data() + buffer.size();
            for (unsigned int ind = 0; ptr != end; ++ptr, ++ind)
            {
                ((char *)(description.c_str()))[ind] = *ptr;
            }
            // TRACE("readBinary description: " << description)
            pos += crt_size;

            m_record.setDescription(description);

            if (!_configuration_b)
            {
                RETURN("readerRecordBinary", NO_ERROR)
            }

            crt_size = sizeof(unsigned int);
            size += crt_size;
            buffer.resize(size);
            _file.seekg(_offset);
            _file.read(buffer.data(), buffer.size());
            unsigned int len_configuration;
            memcpy(&len_configuration, buffer.data() + pos, crt_size);
            // TRACE("readBinary len_configuration: " << len_configuration)
            pos += crt_size;

            crt_size = len_configuration * sizeof(char);
            size += crt_size;
            buffer.resize(size);
            _file.seekg(_offset);
            _file.read(buffer.data(), buffer.size());
            std::string configuration;
            configuration.resize(len_configuration);
            ptr = buffer.data() + pos;
            end = buffer.data() + buffer.size();
            for (unsigned int ind = 0; ptr != end; ++ptr, ++ind)
            {
                ((char *)(configuration.c_str()))[ind] = *ptr;
            }
            // TRACE("readBinary configuration: " << configuration)
            pos += crt_size;

            m_record.setConfiguration(configuration);

            RETURN("readerRecordBinary", NO_ERROR)
        }

        /**
         * @brief ...
         * ...
         * @param _key_name: ...
         * @param _data_b: charge juste les donnees
         * @param _description_b: charge la description, positionne _data_b=true
         * @param _configuration_b: charge la configuration, positionne _data_b=true et _description_b=true
         * @param _checksum_data: checksum data file
         * @param _checksum_all: checksum data and mcdf.don files
         */
        int extractRecordBinary(const std::string _key_name,
                                const bool _data_b, const bool _description_b, const bool _configuration_b,
                                const std::size_t _checksum_data = 0, const std::size_t _checksum_all = 0)
        {
            TRACE("obades::extractRecordBinary " << _key_name)
            m_key_name = _key_name;

            std::string add_path_write = "/accumulateBinary/";
            std::string extension = "";
            if (m_accumulated_binary_by_z_ne)
            {
                TRACE("  m_accumulated_binary_by_z_ne = " << m_accumulated_binary_by_z_ne)
                add_path_write += m_class_indexation_hierarchique.GetZ(_key_name) + "/"; 
                extension = "Ne" + m_class_indexation_hierarchique.GetNe(_key_name);
            }
            TRACE("  add_path_write = " << add_path_write)
            TRACE("  extension = " << extension)
            std::string path_write = m_path_write + add_path_write;
            TRACE("  path_write = " << path_write)
            std::string path_write_reference_offset = m_path_write + "/accumulateBinary/" + m_class_indexation_hierarchique.GetZ(_key_name) + "/";
            TRACE("  path_write_reference_offset = " << path_write_reference_offset)

            try
            {
                unsigned int data_file_index;
                std::size_t offset = m_class_indexation_hierarchique.GetReferenceOffset(path_write_reference_offset, _key_name, data_file_index);
                TRACE("extractRecordBinary data_file_index: " << data_file_index << " offset: " << offset);
                
                std::ifstream &fichier = m_class_indexation_hierarchique.GetReadDataFilename(path_write, extension, data_file_index);
                if(!fichier.is_open())
                {
                    ERROR("fichier pas ouvert " << path_write << _key_name)
                    exit(-1);
                }
                fichier.seekg(0, std::ios::end);
                TRACE("extractRecordBinary next: " << fichier.tellg())
                int ierr = readerRecordBinary(offset, fichier, _data_b, _description_b, _configuration_b,
                                              _checksum_data, _checksum_all);
                if (ierr)
                {
                    RETURN("extractRecordBinary", ierr);
                }
                fichier.close();
            }
            catch (...)
            {
                RETURN("extractRecordBinary", ERROR_INDEX_ACCUMULATE_BINARY_FILE_NOT_EXIST);
            }


            RETURN("extractRecordBinary", NO_ERROR);
        }

        /**
         * @brief ...
         * ...
         * Toute évolution dans listRecordBinary necessite une intervention ici dans GetExtractKeyMultiPath/GetPathAccumulateBinary, et vice et versa, pour prendre en compte le nombre de sous arborescence profondeur
         */
        void listRecordBinary()
        {
            TRACE("listRecordBinary")
            std::string sdir = m_path_write + "/accumulateBinary/";
            
            if (!std::filesystem::exists(sdir))
            {
                TRACE("listRecordBinary " << sdir << " NOT EXIST!")
                return;
            }

            struct stat buf;
            stat(sdir.c_str(), &buf);
            if (!S_ISDIR(buf.st_mode))
            {
                TRACE("listRecordBinary " << sdir << " NOT DIRECTORY!")
                return;
            }

            std::filesystem::path dir = sdir;

            std::vector<std::filesystem::path> files_in_directory;
            std::copy(std::filesystem::directory_iterator(dir), std::filesystem::directory_iterator(), std::back_inserter(files_in_directory));
            std::sort(files_in_directory.begin(), files_in_directory.end());

            bool subPath = false;
            // Parcours suivant l'ordre alphanumérique
            for (const auto& entry : files_in_directory) {
                stat(std::string(entry).c_str(), &buf);
                if (!S_ISDIR(buf.st_mode))
                {
                    TRACE("listRecordBinary " << std::string(entry) << " NOT DIRECTORY!")
                    continue;
                }

                TRACE("  > " << std::string(entry))
                const std::string Z = std::string(entry).substr(sdir.size());
                TRACE("    " << Z)
                std::vector<std::filesystem::path> files_in_directory2;
                std::copy(std::filesystem::directory_iterator(entry), std::filesystem::directory_iterator(), std::back_inserter(files_in_directory2));
                std::sort(files_in_directory2.begin(), files_in_directory2.end());
                for (const auto& entry2 : files_in_directory2) {
                    TRACE("  > > " << std::string(entry2))
                    const std::string prefixe = std::string(entry2).substr(std::string(entry).size()).substr(1, 12);
                    TRACE("  > > prefixe " << prefixe)
                    if (prefixe == "tableIndexNe")
                    {
                        const std::string NE = std::string(entry2).substr(std::string(entry).size()).substr(13, 2);
                        TRACE("    " << NE)
                        const std::string z_ne {Z + "/" + NE};
                        TRACE("    " << z_ne)
                        m_class_indexation_hierarchique.List(std::string(entry2), z_ne);
                    }
                }
            }
        }

        /**
         * @brief Dump
         * @return ...
         */
        void
        dump() const
        {
            m_record.dump();
        }

        /**
         * @brief Get ...
         * @return ...
         */
        int getForcOsc(double &_forcOsc) const
        {
            return m_record.getForcOsc(_forcOsc);
        }

        /**
         * @brief Get size
         * @return ...
         */
        size_t getNumberOfRaies() const
        {
            return m_record.getNumberOfRaies();
        }

        /**
         * @brief Get a Raie
         * @param _index: ...
         * @param _enerRy (out): ...
         * @param _proba (out): ...
         * @param _J21Ini (out): ...
         * @param _J21Fin (out): ...
         */
        int getRaie(const size_t _index, float &_enerRy, float &_proba, unsigned char &_J21Ini, unsigned char &_J21Fin) const
        {
            return m_record.getRaie(_index, _enerRy, _proba, _J21Ini, _J21Fin);
        }

        /**
         * @brief Get all Raies
         * @param _enerRy (out): ...
         * @param _proba (out): ...
         * @param _J21Ini (out): ...
         * @param _J21Fin (out): ...
         */
        int getRaies(std::vector<float> &_enerRy, std::vector<float> &_proba, std::vector<unsigned char> &_J21Ini, std::vector<unsigned char> &_J21Fin) const
        {
            return m_record.getRaies(_enerRy, _proba, _J21Ini, _J21Fin);
        }

        /**
         * @brief Get all Raies
         * @param _energyMin (in): ...
         * @param _energyMax (in): ...
         * @param _enerRy (out): ...
         * @param _proba (out): ...
         * @param _J21Ini (out): ...
         * @param _J21Fin (out): ...
         */
        int getRaies(const float _energyMin, const float _energyMax, std::vector<float> &_enerRy, std::vector<float> &_proba, std::vector<unsigned char> &_J21Ini, std::vector<unsigned char> &_J21Fin) const
        {
            return m_record.getRaies(_energyMin, _energyMax, _enerRy, _proba, _J21Ini, _J21Fin);
        }

    private:
        // accumulate last name base
        std::string m_key_name;

        /**
         * @brief ...
         * ...
         * @param _data_b: charge juste les donnees
         * @param _description_b: charge la description, positionne _data_b=true
         * @param _configuration_b: charge la configuration, positionne _data_b=true et _description_b=true
         */
        int reload(const bool _data_b, const bool _description_b, const bool _configuration_b)
        {
            TRACE("reload " << m_key_name)
            if (m_key_name == "")
            {
                RETURN("reload", 1)
            }
            int ierr = extractRecordBinary(m_key_name, _data_b, _description_b, _configuration_b);
            RETURN("reload", ierr)
        }

    public:
        /**
         * @brief ...
         * @param _reload: s'autorise a recharger si possible la base pour acceder a description
         * @return  ...
         */
        const std::string getDescription(bool _reload = true)
        {
            std::string description = m_record.getDescription();
            if (description.empty() && _reload)
            {
                reload(true, true, false);
                description = m_record.getDescription();
            }
            return description;
        }

        /**
         * @brief ...
         * @param _reload: s'autorise a recharger si possible la base pour acceder a configuration
         * @return  ...
         */
        const std::string getConfiguration(bool _reload = true)
        {
            std::string configuration = m_record.getConfiguration();
            if (configuration.empty() && _reload)
            {
                reload(true, true, true);
                configuration = m_record.getConfiguration();
            }
            return configuration;
        }

        /**
         * @brief Get compute three moment
         * @param _m1 (out): ...
         * @param _m2 (out): ...
         * @param _m3 (out): ...
         */
        int getMoment(float &_m1, float &_m2, float &_m3) const
        {
            return m_record.getMoment(_m1, _m2, _m3);
        }

        /**
         * @brief Open Mcdf base and return Saphyr information
         * @param _z (in): ...
         * @param _ne (in): ...
         * @param _nl (in): 2 values
         * @param _nlq0 (in): last values mustn't be null after offset _nl[1]
         * @param _nlq1 (in): last values mustn't be null
         * @param _forcOsc (out): ...
         * @param _enerRy (out): _nbRays values
         * @param _proba (out): _nbRays values
         * @param _J21Ini (out): _nbRays values
         * @param _J21Fin (out): _nbRays values
         * @param _m1 (out): ...
         * @param _m2 (out): ...
         * @param _m3 (out): ...
         * @param _newMcdfDb (in): ...
         */
        int
        mcdf_for_saphyr(
            const int _z,
            const int _ne,
            const std::vector<int> &_nl,
            const std::vector<int> &_nlq0,
            const std::vector<int> &_nlq1,
            double &_forcOsc,
            std::vector<float> &_enerRy,
            std::vector<float> &_proba,
            std::vector<int> &_J21Ini,
            std::vector<int> &_J21Fin,
            float &_m1,
            float &_m2,
            float &_m3,
            const bool _newMcdfDb
        )
        {
            int ierr = 0;

            std::string key_name;

            // Est-ce que _nl[0] doit être forcement plus petit que _nl[1] ?
            if (_nl.size() != 2)
            {
                ERROR("mcdf_for_saphyr nl must be of size equal 2!")
                RETURN("mcdf_for_saphyr", 1)
            }

            key_name += string_format("%02d/%02d/%02d%02d/", _z, _ne, _nl[0], _nl[1]);
            // On teste ici que le dernier élément du tableau _nlq0 est non nul.
            // Peut-on vérifier d'autres caractèristiques sur _nlq0 ?
            int last = 0;
            unsigned int layer = 0;
            for (const auto &value : _nlq0)
            {
                last = value;
                key_name += string_format("%02d", value);
                ++layer;
            }
            TRACE("mcdf_for_saphyr key: " << key_name);
            // if (layer > _nl[1] && last == 0)
            // {
            //     ERROR("mcdf_for_saphyr nlq0 musn't be finish by null beyond _nl[1]!")
            //     RETURN("mcdf_for_saphyr", 1)
            // }

            // load just data, non load description and configuration compute
            ierr = extractRecordBinary(key_name, true, false, false, false);
            if (ierr)
            {
                if (_newMcdfDb)
                {
                    // _nlq1 est indispensable seulement pour lancer un calcul
                    if (_nlq1.size() != 0)
                    {
                        // Qu'est-ce qu'on peut valider de _nlq1 par rapport à _nlq0 ?
                        // Si la commande de production d'un McdfDb existe alors
                        // il faudra lancer la commande sinon
                        ERROR("mcdf_for_saphyr don't define McdfDbCommand in configuration!")
                        RETURN("mcdf_for_saphyr", 1)
                    }
                }
                // Si la commande de production d'un McdfDb existe, on pourrait
                // ajouter un message pour informer que cela est possible en
                // mettant _newMcdfDb = true et définissant _nlq1
                RETURN("mcdf_for_saphyr", 1)
            }

            // atMass pourrait etre retourné

            ierr = m_record.getForcOsc(_forcOsc);
            if (ierr)
            {
                RETURN("mcdf_for_saphyr", 2)
            }
            ierr = m_record.getRaies(_enerRy, _proba, _J21Ini, _J21Fin);
            if (ierr)
            {
                RETURN("mcdf_for_saphyr", 3)
            }
            ierr = m_record.getMoment(_m1, _m2, _m3);
            if (ierr)
            {
                RETURN("mcdf_for_saphyr", 4)
            }
            RETURN("mcdf_for_saphyr", 0)
        }

        /**
         * @brief Open Mcdf base and return Saphyr information
         * @param _z (in): ...
         * @param _ne (in): ...
         * @param _nl (in): 2 values
         * @param _s_nlq0 (in): ...
         * @param _nlq0 (in): _s_nlq0 values, last values mustn't be null after offset _nl[1]
         * @param _s_nlq1 (in): ...
         * @param _nlq1 (in): _s_nlq1 values, last values mustn't be null
         * @param _forcOsc (out): ...
         * @param _s_nbrays (in): ...
         * @param _enerRy (out): _s_nbrays values
         * @param _proba (out): _s_nbrays values
         * @param _J21Ini (out): _s_nbrays values
         * @param _J21Fin (out): _s_nbrays values
         * @param _m1 (out): ...
         * @param _m2 (out): ...
         * @param _m3 (out): ...
         * @param _newMcdfDb (in): ...
         */
        void
        mcdf_for_saphyr_binding_c(
            const int  *_z,
            const int  *_ne,
            const int  *_nl,
            const int  *_s_nlq0,
            const int  *_nlq0,
            const int  *_s_nlq1,
            const int  *_nlq1,
            double     *_forcOsc,
            int        *_s_nbrays,
            float     **_enerRy,
            float     **_proba,
            int       **_J21Ini,
            int       **_J21Fin,
            float      *_m1,
            float      *_m2,
            float      *_m3,
            const int  *_newMcdfDb,
            int        *_ierr
        ) {
            std::vector<int> nl {_nl[0], _nl[1]};

            std::vector<int> nlq0(*_s_nlq0);
            for(unsigned int i = 0; i < *_s_nlq0; ++i)
            {
                nlq0[i] = _nlq0[i];
            }

            std::vector<int> nlq1(*_s_nlq1);
            for(unsigned int i = 0; i < *_s_nlq1; ++i)
            {
                nlq1[i] = _nlq1[i];
            }

            std::vector<float> enerRy;
            std::vector<float> proba;
            std::vector<int> J21Ini;
            std::vector<int> J21Fin;

            *_ierr = this->mcdf_for_saphyr(*_z, *_ne, nl, nlq0, nlq1, *_forcOsc, enerRy, proba, J21Ini,
                                    J21Fin, *_m1, *_m2, *_m3, (*_newMcdfDb != 0));

            *_s_nbrays = enerRy.size();
            assert(*_s_nbrays == proba.size());
            assert(*_s_nbrays == J21Ini.size());
            assert(*_s_nbrays == J21Fin.size());
            *_enerRy = (float*)malloc(*_s_nbrays*sizeof(float));
            *_proba = (float*)malloc(*_s_nbrays*sizeof(float));
            *_J21Ini = (int*)malloc(*_s_nbrays*sizeof(int));
            *_J21Fin = (int*)malloc(*_s_nbrays*sizeof(int));
            for(int iRay = 0; iRay < *_s_nbrays; ++iRay)
            {
                (*_enerRy)[iRay] = enerRy[iRay];
                (*_proba)[iRay] = proba[iRay];
                (*_J21Ini)[iRay] = J21Ini[iRay];
                (*_J21Fin)[iRay] = J21Fin[iRay];
            }
        }
    };

#undef TRACE
}

#endif // OBADES_H
