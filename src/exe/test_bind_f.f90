program test_binding_f_obades

use obades

implicit none
!
! declaration
! -------------
character(len=*), parameter :: filename = "03/01/0103/010000000000000000000000000000000000000000"
character(len=*), parameter :: path_r   = "data/ascii"
character(len=*), parameter :: path_w   = "temporary"
logical(kind=1),  parameter :: trace    = .TRUE.

integer*1 :: j21ini
integer*1 :: j21Fin
integer :: index
real*4  :: energy
real*4  :: proba
real*8  :: forcOsc

integer :: z
integer :: ne
integer :: nl(2)
integer, allocatable :: nlq0(:)
integer, allocatable :: nlq1(:)
real, pointer :: enerRyS(:)
real, pointer :: probaS(:)
integer, pointer :: j21iniS(:)
integer, pointer :: j21finS(:)
real :: m1
real :: m2
real :: m3
logical :: newMcdfDB
integer :: ierr

real*8 expected_forcOsc

!
! assignation
! ------------
j21ini    = 0
j21Fin    = 0
index     = 0
energy    = 2.3
proba     = 2.3
forcOsc   = 2.3

z = 3
ne = 1
nl(1) = 1
nl(2) = 3
allocate(nlq0(3), nlq1(3))
nlq0(1) = 1
nlq0(2) = 0
nlq0(3) = 0
nlq1(1) = 1
nlq1(2) = 1
nlq1(3) = 1
newMcdfDB = .FALSE.
ierr = 0

! appel obades
! ------------
call Create(path_r, path_w, trace)

call Load(filename)

print*, 'BEFORE CALLING GETFORCOSC : forcOsc = ', forcOsc
call GetForcOsc(index, forcOsc)
print*, 'AFTER  CALLING GETFORCOSC : forcOsc = ', forcOsc

print*, 'BEFORE CALLING GETRAIE : energy = ', energy, '  proba = ', proba, ' J21Ini = ', J21Ini, ' J21Fin = ', J21Fin
call GetRaie(index, energy, proba, j21ini, j21Fin)
print*, 'AFTER  CALLING GETRAIE : energy = ', energy, '  proba = ', proba, ' J21Ini = ', J21Ini, ' J21Fin = ', J21Fin

forcOsc = 0.
print*, 'BEFORE CALLING GETMCDF : '
call GetMcdf(z, ne, nl, nlq0, nlq1, forcOsc, enerRyS, probaS, J21IniS, J21FinS, m1, m2, m3, newMcdfDB, ierr)
print*, 'AFTER  CALLING GETMCDF'

if ( ABS(forcOsc - 0.119835531950193) .GT. 1E14) then
    print*, "### FATAL ERROR ### forcOsc (", forcOsc, ") not exepted (0.11983553195019300000) ! Ecart ",&
&           ABS(forcOsc - 0.11983553195019300000)
!  call exit(-1)
endif

if ( size(enerRyS) .NE. 2 ) then
  print*, "### FATAL ERROR ### size(enerRyS) ", size(enerRyS), " not exepted (2) !"
!  call exit(-1)
end if
if ( ABS(enerRyS(1) - 6.750061E+00) .GT. 1E14 ) then
  print*, "### FATAL ERROR ### enerRyS(1) ", enerRyS(1), " not exepted (6.750061E+00) !", ABS(enerRyS(1) - 6.750061E+00)
!  call exit(-1)
end if
if ( ABS(enerRyS(2) - 6.750331E+00) .GT. 1E14 ) then
  print*, "### FATAL ERROR ### enerRyS(2) ", enerRyS(2), " not exepted (6.750331E+00) !", ABS(enerRyS(2) - 6.750331E+00)
!  call exit(-1)
end if

if ( size(probaS) .NE. 2 ) then
  print*, "### FATAL ERROR ### size(probaS) ", size(probaS), " not exepted (2) !"
!  call exit(-1)
end if
if ( ABS(probaS(1) - 3.33268404E-01) .GT. 1E14 ) then
  print*, "### FATAL ERROR ### probaS(1) ", probaS(1), " not exepted (3.33268404E-01) !"
!  call exit(-1)
end if
if ( ABS(probaS(2) - 6.667316596-01) .GT. 1E14 ) then
  print*, "### FATAL ERROR ### probaS(2) ", probaS(2), " not exepted (6.66731596E-01) !"
!  call exit(-1)
end if

if ( size(J21IniS) .NE. 2 ) then
  print*, "### FATAL ERROR ### size(J21IniS) ", size(J21IniS), " not exepted (2) !"
!  call exit(-1)
end if
if (J21IniS(1) .NE. 2) then
  print*, "### FATAL ERROR ### J21IniS(1) ", J21IniS(1), " not exepted (2) !"
!  call exit(-1)
end if
if (J21IniS(2) .NE. 2) then
  print*, "### FATAL ERROR ### J21IniS(2) ", J21IniS(2), " not exepted (2) !"
!  call exit(-1)
end if

if ( size(J21FinS) .NE. 2 ) then
  print*, "### FATAL ERROR ### size(J21FinS) ", size(J21FinS), " not exepted (2) !"
!  call exit(-1)
end if
if (J21FinS(1) .NE. 255) then
  print*, "### FATAL ERROR ### J21FinS(1) ", J21FinS(1), " not exepted (255) !"
!  call exit(-1)
end if
if (J21FinS(2) .NE. 255) then
  print*, "### FATAL ERROR ### J21FinS(2) ", J21FinS(2), " not exepted (255) !"
!  call exit(-1)
end if

if ( ABS(m1 - 6.75024128) .GT. 5E-6) then
  print*, "### FATAL ERROR ### m1 ", m1, " not exepted (6.75024) ! Ecart ", ABS(m1 - 6.750240)
!  call exit(-1)
end if
if ( ABS(m2 - 1.61853e-08) .GT. 5E-6) then
  print*, "### FATAL ERROR ### m2 ", m2, " not exepted (1.61853e-08) ! Ecart ", ABS(m2 - 1.61853e-08)
!  call exit(-1)
end if
if ( ABS(m3 + 1.47123e-12) .GT. 5E-6) then
  print*, "### FATAL ERROR ### m3 ", m3, " not exepted (-1.47123e-12) ! Ecart ", ABS(m3 + 1.47123e-12)
!  call exit(-1)
end if

print*, 'BEFORE CALLING GETMCDF : '
call GetMcdfClean(enerRyS, probaS, J21IniS, J21FinS)
print*, 'AFTER  CALLING GETMCDF'

print*, 'BEFORE CALLING GETMCDF : '
call GetMcdf(z, ne, nl, nlq0, nlq1, forcOsc, enerRyS, probaS, J21IniS, J21FinS, m1, m2, m3, newMcdfDB, ierr)
print*, 'AFTER  CALLING GETMCDF'

call Destroy()

! test error, decomment next line
! -----------
!call Load(filename)

deallocate(nlq0, nlq1)

stop
!
end program
