// Non Regression Test on C++ class OBaDES
// - read/write ASCII
// - read/write binaire
// - read/write accumulate binaire one file with index json
// with comparaison of
// - checksum
// - value by value

#include "obades.hpp"

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int checked_data_010000(const double _forcOsc,
                        const std::vector<float> &_enerRy,
                        const std::vector<float> &_proba,
                        const std::vector<unsigned char> &_J21Ini,
                        const std::vector<unsigned char> &_J21Fin,
                        const float _m1,
                        const float _m2,
                        const float _m3)
{
    if (std::fabs(_forcOsc - 0.119835531950193) != 0)
    {
        std::cerr << "### FATAL ERROR ### forcOsc (" << _forcOsc << ") != 0.119835531950193" << std::endl;
        return 1;
    }
    if (_enerRy.size() != 2)
    {
        std::cerr << "### FATAL ERROR ### enerRy size (" << _enerRy.size() << ") != 2" << std::endl;
        return 1;
    }
    if (_proba.size() != 2)
    {
        std::cerr << "### FATAL ERROR ### proba size (" << _proba.size() << ") != 2" << std::endl;
        return 1;
    }
    if (_J21Ini.size() != 2)
    {
        std::cerr << "### FATAL ERROR ### J21Ini size (" << _J21Ini.size() << ") != 2" << std::endl;
        return 1;
    }
    if (_J21Fin.size() != 2)
    {
        std::cerr << "### FATAL ERROR ### J21Fin size (" << _J21Fin.size() << ") != 2" << std::endl;
        return 1;
    }
    std::vector<float> waited_enerRy{6.750061E+00, 6.750331E+00};
    std::vector<float> waited_proba{3.332684E-01, 6.667316E-01};
    for (size_t index = 0; index < _enerRy.size(); ++index)
    {
        if (std::fabs(_enerRy[index] - waited_enerRy[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << " enerRy (" << _enerRy[index] << ") != waited " << waited_enerRy[index] << std::endl;
            return 1;
        }
        if (std::fabs(_proba[index] - waited_proba[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << " proba (" << _proba[index] << ") != waited " << waited_proba[index] << std::endl;
            return 1;
        }
        if (std::fabs(_J21Ini[index] - 2) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << " J21Ini (" << _J21Ini[index] << ") != waited " << 2 << std::endl;
            return 1;
        }
        if (std::fabs(_J21Fin[index] - std::numeric_limits<unsigned char>::max()) != 0)
        {
            std::cerr << "### FATAL ERROR ### #" << index << " J21Fin (" << _J21Fin[index] << ") != " << std::numeric_limits<unsigned char>::max() << std::endl;
            return 1;
        }
    }
    float ecart_m1 = std::fabs(_m1 - 6.75024128);
    float ecart_m2 = std::fabs(_m2 - 1.61853e-08);
    float ecart_m3 = std::fabs(_m3 - -1.47123e-12);
    if (ecart_m1 > 5e-6 || ecart_m2 > 5e-6 || ecart_m3 > 5e-6)
    {
        std::cerr << "m1: " << _m1 << "(dm1:" << ecart_m1 << ") m2: " << _m2 << "(dm2:" << ecart_m2 << ") m3: " << _m3 << "(dm3:" << ecart_m3 << ")" << std::endl;
        return 1;
    }
    return 0;
}

int checked_data_010000(const double _forcOsc,
                                      const std::vector<float> &_enerRy,
                                      const std::vector<float> &_proba,
                                      const std::vector<int> &_J21Ini,
                                      const std::vector<int> &_J21Fin,
                                      const float _m1,
                                      const float _m2,
                                      const float _m3)
{
    assert(_J21Ini.size() == _J21Fin.size());
    std::vector<unsigned char> J21Ini(_J21Ini.size());
    std::vector<unsigned char> J21Fin(_J21Fin.size());
    size_t nbRays = _J21Ini.size();
    for(size_t iRay = 0; iRay < nbRays; ++iRay)
    {
        J21Ini[iRay] = (unsigned char)_J21Ini[iRay]; 
        J21Fin[iRay] = (unsigned char)_J21Fin[iRay]; 
    }

    return checked_data_010000(_forcOsc, _enerRy, _proba, J21Ini, J21Fin, _m1, _m2, _m3);
}


//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int checked_data_01000000000000000001(const double _forcOsc,
                                      const std::vector<float> &_enerRy,
                                      const std::vector<float> &_proba,
                                      const std::vector<unsigned char> &_J21Ini,
                                      const std::vector<unsigned char> &_J21Fin,
                                      const float _m1,
                                      const float _m2,
                                      const float _m3)
{
    if (std::fabs(_forcOsc - 1.67708823428419) != 0)
    {
        std::cerr << "### FATAL ERROR ### forcOsc (" << _forcOsc << ") != 1.67708823428419" << std::endl;
        return 1;
    }
    if (_enerRy.size() != 36)
    {
        std::cerr << "### FATAL ERROR ### enerRy size (" << _enerRy.size() << ") != 36" << std::endl;
        return 1;
    }
    if (_proba.size() != 36)
    {
        std::cerr << "### FATAL ERROR ### proba size (" << _proba.size() << ") != 36" << std::endl;
        return 1;
    }
    if (_J21Ini.size() != 36)
    {
        std::cerr << "### FATAL ERROR ### J21Ini size (" << _J21Ini.size() << ") != 36" << std::endl;
        return 1;
    }
    if (_J21Fin.size() != 36)
    {
        std::cerr << "### FATAL ERROR ### J21Fin size (" << _J21Fin.size() << ") != 36" << std::endl;
        return 1;
    }
    std::vector<float> waited_enerRy{6.746132E+00,
                                     6.746133E+00,
                                     6.746134E+00,
                                     6.746134E+00,
                                     6.746349E+00,
                                     6.746351E+00,
                                     6.746351E+00,
                                     6.746373E+00,
                                     6.746375E+00,
                                     6.746375E+00,
                                     6.746376E+00,
                                     6.746401E+00,
                                     6.746403E+00,
                                     6.746404E+00,
                                     6.750726E+00,
                                     6.750727E+00,
                                     6.750728E+00,
                                     6.750728E+00,
                                     6.750806E+00,
                                     6.750808E+00,
                                     6.750809E+00,
                                     6.750927E+00,
                                     6.752008E+00,
                                     6.752010E+00,
                                     6.752011E+00,
                                     6.752850E+00,
                                     6.752852E+00,
                                     6.752852E+00,
                                     6.752853E+00,
                                     6.752930E+00,
                                     6.752932E+00,
                                     6.752933E+00,
                                     6.753001E+00,
                                     6.753549E+00,
                                     6.753551E+00,
                                     6.753552E+00};
    std::vector<float> waited_proba{7.170648E-02,
                                    1.691468E-04,
                                    1.508952E-04,
                                    1.126021E-02,
                                    1.184782E-03,
                                    5.237502E-02,
                                    5.934179E-03,
                                    8.302897E-03,
                                    5.658910E-03,
                                    7.307874E-03,
                                    6.201979E-02,
                                    8.678394E-04,
                                    1.013731E-01,
                                    4.843118E-03,
                                    1.555327E-03,
                                    7.729052E-02,
                                    7.833849E-05,
                                    4.427701E-03,
                                    2.747748E-02,
                                    5.604278E-03,
                                    7.407809E-02,
                                    1.309581E-01,
                                    7.883057E-02,
                                    1.148943E-04,
                                    2.822739E-02,
                                    1.722403E-03,
                                    2.255135E-04,
                                    7.583437E-02,
                                    5.591241E-03,
                                    5.674459E-03,
                                    7.076159E-03,
                                    4.679821E-02,
                                    3.572680E-02,
                                    5.268930E-02,
                                    4.561479E-05,
                                    6.819009E-03};
    std::vector<unsigned char> waited_J21Ini{7,
                                             5,
                                             9,
                                             7,
                                             7,
                                             5,
                                             7,
                                             7,
                                             5,
                                             9,
                                             7,
                                             7,
                                             9,
                                             7,
                                             7,
                                             5,
                                             9,
                                             7,
                                             7,
                                             9,
                                             7,
                                             9,
                                             7,
                                             9,
                                             7,
                                             7,
                                             5,
                                             9,
                                             7,
                                             7,
                                             5,
                                             7,
                                             5,
                                             7,
                                             5,
                                             7};
    for (size_t index = 0; index < _enerRy.size(); ++index)
    {
        if (std::fabs(_enerRy[index] - waited_enerRy[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << " enerRy (" << _enerRy[index] << ") != waited " << waited_enerRy[index] << std::endl;
            return 1;
        }
        if (std::fabs(_proba[index] - waited_proba[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << " proba (" << _proba[index] << ") != waited " << waited_proba[index] << std::endl;
            return 1;
        }
        if (std::fabs(_J21Ini[index] - waited_J21Ini[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << " J21Ini (" << _J21Ini[index] << ") != waited " << waited_J21Ini[index] << std::endl;
            return 1;
        }
        if (std::fabs(_J21Fin[index] - std::numeric_limits<unsigned char>::max()) != 0)
        {
            std::cerr << "### FATAL ERROR ### #" << index << " J21Fin (" << _J21Fin[index] << ") != " << std::numeric_limits<unsigned char>::max() << std::endl;
            return 1;
        }
    }
    if (std::fabs(1 - 6.74999 / _m1) > 5e-6 || std::fabs(1 - 7.439e-06 / _m2) > 5e-6 || std::fabs(1 - (-8.28746e-09) / _m3) > 5e-6)
    {
        std::cerr << "m1: " << _m1 << " m2: " << _m2 << " m3: " << _m3 << std::endl;
        std::cerr << "execpted m1: " << 6.74999 << " m2: " << 7.439e-06 << " m3: " << -8.28746e-09 << std::endl;
        return 1;
    }
    return 0;
}

int checked_data_01000000000000000001(const double _forcOsc,
                                      const std::vector<float> &_enerRy,
                                      const std::vector<float> &_proba,
                                      const std::vector<int> &_J21Ini,
                                      const std::vector<int> &_J21Fin,
                                      const float _m1,
                                      const float _m2,
                                      const float _m3)
{
    assert(_J21Ini.size() == _J21Fin.size());
    std::vector<unsigned char> J21Ini(_J21Ini.size());
    std::vector<unsigned char> J21Fin(_J21Fin.size());
    size_t nbRays = _J21Ini.size();
    for(size_t iRay = 0; iRay < nbRays; ++iRay)
    {
        J21Ini[iRay] = (unsigned char)_J21Ini[iRay]; 
        J21Fin[iRay] = (unsigned char)_J21Fin[iRay]; 
    }

    return checked_data_01000000000000000001(_forcOsc, _enerRy, _proba, J21Ini, J21Fin, _m1, _m2, _m3);
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int main(int _argc, const char *_argv[])
{
    std::cerr << "In progress exe/test_mcdf_for_saphyr..." << std::endl;
    //-----------------------------------
    std::cerr << "argc: " << _argc << std::endl;
    std::cerr << "argv: " << _argv[0] << std::endl;
    //-----------------------------------
    int ierr = 0;
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    obades::obades fdobades("../ii", "temporary", false);
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 03/02/0103/010000000000000000010000000000000000000000" << std::endl;
    {
        // "03/02/0103/010000000000000000010000000000000000000000"
        int z = 3;
        int ne = 2;
        std::vector<int> nl{1, 3};
        //                    01 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00
        std::vector<int> nlq0{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        std::vector<int> nlq1{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // no consistent values
        double forcOsc = 0.;
        std::vector<float> enerRy;
        std::vector<float> proba;
        std::vector<int> J21Ini;
        std::vector<int> J21Fin;
        float m1;
        float m2;
        float m3;
        bool newMcdfDB = false;

        ierr = fdobades.mcdf_for_saphyr(z, ne, nl, nlq0, nlq1, forcOsc, enerRy, proba, J21Ini, J21Fin, m1, m2, m3, newMcdfDB);
        if (ierr != obades::obades::NO_ERROR)
        {
            std::cerr << "### FATAL ERROR ### mcdf_for_saphyr" << std::endl;
            exit(1);
        }
        ierr = checked_data_01000000000000000001(forcOsc,
                                                 enerRy,
                                                 proba,
                                                 J21Ini,
                                                 J21Fin,
                                                 m1, m2, m3);
        if (ierr) // if error
        {
            std::cerr << "### FATAL ERROR ### checked_data_01000000000000000001" << std::endl;
            exit(1);
        }
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- unknow" << std::endl;
    {
        int z = 5;
        int ne = 3;
        std::vector<int> nl{1, 3};
        std::vector<int> nlq0{1, 0, 1};
        std::vector<int> nlq1{1, 0, 1}; // no consistent values
        double forcOsc = 0.;
        std::vector<float> enerRy;
        std::vector<float> proba;
        std::vector<int> J21Ini;
        std::vector<int> J21Fin;
        float m1;
        float m2;
        float m3;
        bool newMcdfDB = false;

        ierr = fdobades.mcdf_for_saphyr(z, ne, nl, nlq0, nlq1, forcOsc, enerRy, proba, J21Ini, J21Fin, m1, m2, m3, newMcdfDB);
        if (ierr == obades::obades::NO_ERROR) // if not error
        {
            std::cerr << "### FATAL ERROR ### wiated mcdf_for_saphyr in error!" << std::endl;
            exit(1);
        }
        else
        {
            std::cerr << "... but checked this unknow test (" << ierr << ")!" << std::endl;
        }
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- failed" << std::endl;
    {
        int z = 3;
        int ne = 2;
        std::vector<int> nl{1, 3};
        //                    01 00 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00
        std::vector<int> nlq0{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        std::vector<int> nlq1{1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // no consistent values
        double forcOsc = 0.;
        std::vector<float> enerRy;
        std::vector<float> proba;
        std::vector<int> J21Ini;
        std::vector<int> J21Fin;
        float m1;
        float m2;
        float m3;
        bool newMcdfDB = false;

        ierr = fdobades.mcdf_for_saphyr(z, ne, nl, nlq0, nlq1, forcOsc, enerRy, proba, J21Ini, J21Fin, m1, m2, m3, newMcdfDB);
        std::cerr << "forcOsc: " << forcOsc << std::endl;
        size_t nbRays =  enerRy.size();
        std::cerr << "nb rays: " << nbRays << std::endl;
        for(size_t iRay = 0; iRay < nbRays; ++iRay)
        {
            std::cerr << "#" << iRay << std::endl;
            std::cerr << "   enerRy : " << enerRy[iRay] << std::endl;
            std::cerr << "   proba  : " << proba[iRay] << std::endl;
            std::cerr << "   J21Ini : " << J21Ini[iRay] << std::endl;
            std::cerr << "   J21Fin : " << J21Fin[iRay] << std::endl;
        }
        std::cerr << "m1: " << m1 << std::endl;
        std::cerr << "m2: " << m2 << std::endl;
        std::cerr << "m3: " << m3 << std::endl;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 010000" << std::endl;
    {
        // "03/01/0103/010000"
        int z = 3;
        int ne = 1;
        std::vector<int> nl{1, 3};
        //                    54  32  10 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
        std::vector<int> nlq0{54, 32, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        std::vector<int> nlq1{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // no consistent values
        double forcOsc = 0.;
        std::vector<float> enerRy;
        std::vector<float> proba;
        std::vector<int> J21Ini;
        std::vector<int> J21Fin;
        float m1;
        float m2;
        float m3;
        bool newMcdfDB = false;

        ierr = fdobades.mcdf_for_saphyr(z, ne, nl, nlq0, nlq1, forcOsc, enerRy, proba, J21Ini, J21Fin, m1, m2, m3, newMcdfDB);
        if (ierr != obades::obades::NO_ERROR) // if not error
        {
            std::cerr << "### FATAL ERROR ### mcdf_for_saphyr" << std::endl;
            exit(1);
        }
        ierr = checked_data_010000(forcOsc,
                                   enerRy,
                                   proba,
                                   J21Ini,
                                   J21Fin,
                                   m1, m2, m3);
        if (ierr)
        {
            std::cerr << "### FATAL ERROR ### checked_data_010000" << std::endl;
            exit(1);
        }
    }
    //-----------------------------------
    std::cout << "...Finish" << std::endl;
    //-----------------------------------
    return 0;
}
