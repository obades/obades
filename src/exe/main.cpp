// Non Regression Test on C++ class OBaDES
// - read/write ASCII
// - read/write binaire
// - read/write accumulate binaire one file with index json
// with comparaison of
// - checksum
// - value by value

#include "obades.hpp"

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int checked_data_010000(const obades::obades &_fdobades)
{
    // _fdobades.dump();
    double forcOsc;
    int ierr = _fdobades.getForcOsc(forcOsc);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### getForcOsc" << std::endl;
        return 1;
    }
    if (std::fabs(forcOsc - 0.119835531950193) != 0)
    {
        std::cerr << "### FATAL ERROR ### forcOsc != 0.119835531950193" << std::endl;
        return 1;
    }
    if (_fdobades.getNumberOfRaies() != 2)
    {
        std::cerr << "### FATAL ERROR ### getNumberOfRaies != 2" << std::endl;
        return 1;
    }
    std::vector<float> waited_enerRy{6.750061E+00, 6.750331E+00};
    std::vector<float> waited_proba{3.332684E-01, 6.667316E-01};
    for (size_t index = 0; index < _fdobades.getNumberOfRaies(); ++index)
    {
        float enerRy, proba;
        unsigned char J21Ini, J21Fin;
        ierr = _fdobades.getRaie(index, enerRy, proba, J21Ini, J21Fin);
        if (ierr)
        {
            std::cerr << "### FATAL ERROR ### getRaie" << std::endl;
            return 1;
        }
        if (std::fabs(enerRy - waited_enerRy[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << "enerRy: " << enerRy << " != waited " << waited_enerRy[index] << std::endl;
            return 1;
        }
        if (std::fabs(proba - waited_proba[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << "proba: " << proba << " != waited " << waited_proba[index] << std::endl;
            return 1;
        }
        if (J21Ini != 2)
        {
            std::cerr << "### FATAL ERROR ### #" << index << " " << J21Ini << " != 2" << std::endl;
            return 1;
        }
        if (J21Fin != std::numeric_limits<unsigned char>::max())
        {
            std::cerr << "### FATAL ERROR ### #" << index << " " << J21Fin << " != " << std::numeric_limits<unsigned char>::max() << std::endl;
            return 1;
        }
    }
    float m1 = 0., m2 = 0., m3 = 0.;
    ierr = _fdobades.getMoment(m1, m2, m3);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### getMoment return 1 if sum(proba) diff de 1" << std::endl;
        return 1;
    }
    if (std::fabs(1 - 6.75024 / m1) > 5e-6 || std::fabs(1 - 1.61853e-08 / m2) > 5e-6 || std::fabs(1 - (-1.47123e-12) / m3) > 5e-6)
    {
        std::cerr << "m1: " << m1 << " m2: " << m2 << " m3: " << m3 << std::endl;
        return 1;
    }
    return 0;
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int checked_data_01000000000000000001(const obades::obades &_fdobades)
{
    // _fdobades.dump();
    double forcOsc;
    int ierr = _fdobades.getForcOsc(forcOsc);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### getForcOsc" << std::endl;
        return 1;
    }
    if (std::fabs(forcOsc - 1.67708823428419) != 0)
    {
        std::cerr << "### FATAL ERROR ### forcOsc != 1.67708823428419" << std::endl;
        return 1;
    }
    if (_fdobades.getNumberOfRaies() != 36)
    {
        std::cerr << "### FATAL ERROR ### getNumberOfRaies != 36" << std::endl;
        return 1;
    }
    std::vector<float> waited_enerRy{6.746132E+00,
                                     6.746133E+00,
                                     6.746134E+00,
                                     6.746134E+00,
                                     6.746349E+00,
                                     6.746351E+00,
                                     6.746351E+00,
                                     6.746373E+00,
                                     6.746375E+00,
                                     6.746375E+00,
                                     6.746376E+00,
                                     6.746401E+00,
                                     6.746403E+00,
                                     6.746404E+00,
                                     6.750726E+00,
                                     6.750727E+00,
                                     6.750728E+00,
                                     6.750728E+00,
                                     6.750806E+00,
                                     6.750808E+00,
                                     6.750809E+00,
                                     6.750927E+00,
                                     6.752008E+00,
                                     6.752010E+00,
                                     6.752011E+00,
                                     6.752850E+00,
                                     6.752852E+00,
                                     6.752852E+00,
                                     6.752853E+00,
                                     6.752930E+00,
                                     6.752932E+00,
                                     6.752933E+00,
                                     6.753001E+00,
                                     6.753549E+00,
                                     6.753551E+00,
                                     6.753552E+00};
    std::vector<float> waited_proba{7.170648E-02,
                                    1.691468E-04,
                                    1.508952E-04,
                                    1.126021E-02,
                                    1.184782E-03,
                                    5.237502E-02,
                                    5.934179E-03,
                                    8.302897E-03,
                                    5.658910E-03,
                                    7.307874E-03,
                                    6.201979E-02,
                                    8.678394E-04,
                                    1.013731E-01,
                                    4.843118E-03,
                                    1.555327E-03,
                                    7.729052E-02,
                                    7.833849E-05,
                                    4.427701E-03,
                                    2.747748E-02,
                                    5.604278E-03,
                                    7.407809E-02,
                                    1.309581E-01,
                                    7.883057E-02,
                                    1.148943E-04,
                                    2.822739E-02,
                                    1.722403E-03,
                                    2.255135E-04,
                                    7.583437E-02,
                                    5.591241E-03,
                                    5.674459E-03,
                                    7.076159E-03,
                                    4.679821E-02,
                                    3.572680E-02,
                                    5.268930E-02,
                                    4.561479E-05,
                                    6.819009E-03};
    std::vector<unsigned char> waited_J21Ini{7,
                                             5,
                                             9,
                                             7,
                                             7,
                                             5,
                                             7,
                                             7,
                                             5,
                                             9,
                                             7,
                                             7,
                                             9,
                                             7,
                                             7,
                                             5,
                                             9,
                                             7,
                                             7,
                                             9,
                                             7,
                                             9,
                                             7,
                                             9,
                                             7,
                                             7,
                                             5,
                                             9,
                                             7,
                                             7,
                                             5,
                                             7,
                                             5,
                                             7,
                                             5,
                                             7};
    for (size_t index = 0; index < _fdobades.getNumberOfRaies(); ++index)
    {
        float enerRy, proba;
        unsigned char J21Ini, J21Fin;
        ierr = _fdobades.getRaie(index, enerRy, proba, J21Ini, J21Fin);
        if (ierr)
        {
            std::cerr << "### FATAL ERROR ### getRaie" << std::endl;
            return 1;
        }
        if (std::fabs(enerRy - waited_enerRy[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << "enerRy: " << enerRy << " != waited " << waited_enerRy[index] << std::endl;
            return 1;
        }
        if (std::fabs(proba - waited_proba[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << "proba: " << proba << " != waited " << waited_proba[index] << std::endl;
            return 1;
        }
        if (std::fabs(J21Ini - waited_J21Ini[index]) != 0)
        {
            std::cerr << "### FATAL ERROR ### # " << index << "J21Ini: " << J21Ini << " != waited " << waited_J21Ini[index] << std::endl;
            return 1;
        }
        if (J21Fin != std::numeric_limits<unsigned char>::max())
        {
            std::cerr << "### FATAL ERROR ### #" << index << " " << J21Fin << " != " << std::numeric_limits<unsigned char>::max() << std::endl;
            return 1;
        }
    }
    float m1 = 0., m2 = 0., m3 = 0.;
    ierr = _fdobades.getMoment(m1, m2, m3);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### getMoment return 1 if sum(proba) diff de 1" << std::endl;
        return 1;
    }
    if (std::fabs(1 - 6.74999 / m1) > 5e-6 || std::fabs(1 - 7.439e-06 / m2) > 5e-6 || std::fabs(1 - (-8.28746e-09) / m3) > 5e-6)
    {
        std::cerr << "m1: " << m1 << " m2: " << m2 << " m3: " << m3 << std::endl;
        return 1;
    }
    return 0;
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int main(int _argc, const char *_argv[])
{
    std::cerr << "In progress exe/main..." << std::endl;

    std::cerr << "argc: " << _argc << std::endl;
    std::cerr << "argv: " << _argv[0] << std::endl;

    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 010000" << std::endl;
    obades::obades fdobades("data/ascii", "temporary", false);
    int ierr = fdobades.readAscii("03/01/0103/010000000000000000000000000000000000000000",
        7517112906371251945u,
        16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readAscii" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.writeAscii("03/01/0103/010000999000000000000000000000000000000000",
    7517112906371251945u,
        16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### writeAscii 03/01/0103/010000999" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.writeAscii("03/01/0103/010000000000000000000000000000000000000000",
    7517112906371251945u,
        16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### writeAscii 03/01/0103/010000" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.readAscii("03/01/0103/010000999000000000000000000000000000000000",
    7517112906371251945u,
        16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readAscii" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.writeBinary("03/01/0103/010000424200000000000000000000000000000000",
    7517112906371251945u,
        16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### writeBinary" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.readBinary("03/01/0103/010000424200000000000000000000000000000000",
    7517112906371251945u,
        16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readBinary" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.writeAscii("03/01/0103/010000696900000000000000000000000000000000",
    7517112906371251945u,
        16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### writeAscii" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.readAscii("03/01/0103/010000696900000000000000000000000000000000",
    7517112906371251945u,
        16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readAscii" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    std::cerr << "Full test checked 010000" << std::endl;
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 01000000000000000001" << std::endl;
    ierr = fdobades.readAscii("03/02/0103/010000000000000000010000000000000000000000",
        13937047830326616237u, 1470069225634614933); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readAscii" << std::endl;
        return 1;
    }
    ierr = checked_data_01000000000000000001(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.writeAscii("03/02/0103/010000000000000000010000000000000000000000A",
        13937047830326616237u, 1470069225634614933); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### writeAscii" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.readAscii("03/02/0103/010000000000000000010000000000000000000000A",
        13937047830326616237u, 1470069225634614933); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readAscii" << std::endl;
        return 1;
    }
    ierr = checked_data_01000000000000000001(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.writeBinary("03/02/0103/010000000000000000010000000000000000000000B",
        13937047830326616237u, 1470069225634614933); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### writeBinary" << std::endl;
        return 1;
    }
    //-----------------------------------
    ierr = fdobades.readBinary("03/02/0103/010000000000000000010000000000000000000000B",
        13937047830326616237u, 1470069225634614933); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readBinary" << std::endl;
        return 1;
    }
    ierr = checked_data_01000000000000000001(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.writeAscii("03/02/0103/010000000000000000010000000000000000000000C",
        13937047830326616237u, 1470069225634614933); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### writeAscii" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    ierr = fdobades.readAscii("03/02/0103/010000000000000000010000000000000000000000C",
        13937047830326616237u, 1470069225634614933); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readAscii" << std::endl;
        return 1;
    }
    ierr = checked_data_01000000000000000001(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-" << std::endl;
    std::cerr << "Full test checked 01000000000000000001" << std::endl;
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Acc 010000" << std::endl;
    ierr = fdobades.readAscii("03/01/0103/010000000000000000000000000000000000000000",
        7517112906371251945u, 16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readAscii First" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.accumulateRecordBinary("03/01/0103/010000000000000000000000000000000000000000",
        7517112906371251945u, 16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### accumulateBinary First" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/01/0103/010000000000000000000000000000000000000000",
        true, true, true,
        7517112906371251945u, 16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### extractRecordBinary First" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Acc 010000000000000000010000000000000000000000" << std::endl;
    ierr = fdobades.readAscii("03/02/0103/010000000000000000010000000000000000000000",
        13937047830326616237u, 1470069225634614933u); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### readAscii Second" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.accumulateRecordBinary("03/02/0103/010000000000000000010000000000000000000000",
        13937047830326616237u, 1470069225634614933u); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### accumulateBinary Second" << std::endl;
        return 1;
    }
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/02/0103/010000000000000000010000000000000000000000",
        true, true, true, 13937047830326616237u, 1470069225634614933u); // 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### extractRecordBinary Second" << std::endl;
        return 1;
    }
    ierr = checked_data_01000000000000000001(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/01/0103/010000",
        true, true, true,
        7517112906371251945u, 16373780333360218784u); // 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### extractRecordBinary First" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/01/0103/010000", true, true, true, 7517112906371251945u, 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### extractRecordBinary First" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    std::string description = fdobades.getDescription(false);
    std::string configuration = fdobades.getConfiguration(false);
    if (description.empty())
    {
        std::cerr << "### FATAL ERROR ### description (" << description << ") is empty!" << std::endl;
        return 1;
    }
    if (configuration.empty())
    {
        std::cerr << "### FATAL ERROR ### configuration (" << configuration << ") is empty!" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/01/0103/010000", true, false, false, 7517112906371251945u, 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### extractRecordBinary First" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    if (!fdobades.getDescription(false).empty())
    {
        std::cerr << "### FATAL ERROR ### description (" << fdobades.getDescription(false) << ") != None" << std::endl;
        return 1;
    }
    else if (fdobades.getDescription() != description)
    {
        std::cerr << "### FATAL ERROR ### description (" << fdobades.getDescription() << ") != (" << description << ")" << std::endl;
        return 1;
    }
    if (!fdobades.getConfiguration(false).empty())
    {
        std::cerr << "### FATAL ERROR ### configuration (" << fdobades.getConfiguration(false) << ") != None" << std::endl;
        return 1;
    }
    else if (fdobades.getConfiguration() != configuration)
    {
        std::cerr << "### FATAL ERROR ### configuration (" << fdobades.getConfiguration() << ") != (" << configuration << ")" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/01/0103/010000", true, true, false, 7517112906371251945u, 12031059765721206653u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### extractRecordBinary First" << std::endl;
        return 1;
    }
    ierr = checked_data_010000(fdobades);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### checked data failed" << std::endl;
        return 1;
    }
    if (fdobades.getDescription(false) != description)
    {
        std::cerr << "### FATAL ERROR ### description (" << fdobades.getDescription(false) << ") != (" << description << ")" << std::endl;
        return 1;
    }
    if (fdobades.getDescription() != description)
    {
        std::cerr << "### FATAL ERROR ### description (" << fdobades.getDescription() << ") != (" << description << ")" << std::endl;
        return 1;
    }
    if (!fdobades.getConfiguration(false).empty())
    {
        std::cerr << "### FATAL ERROR ### configuration (" << fdobades.getConfiguration(false) << ") != None" << std::endl;
        return 1;
    }
    else if (fdobades.getConfiguration() != configuration)
    {
        std::cerr << "### FATAL ERROR ### configuration (" << fdobades.getConfiguration() << ") != (" << configuration << ")" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/02/0103/01000000000000000001", true, true, true, 13937047830326616237u, 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### accumulateBinary Second" << std::endl;
        return 1;
    }
    description = fdobades.getDescription(false);
    configuration = fdobades.getConfiguration(false);
    if (description.empty())
    {
        std::cerr << "### FATAL ERROR ### description (" << description << ") is empty!" << std::endl;
        return 1;
    }
    if (configuration.empty())
    {
        std::cerr << "### FATAL ERROR ### configuration (" << configuration << ") is empty!" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/02/0103/01000000000000000001", true, false, false, 13937047830326616237u, 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### accumulateBinary Second" << std::endl;
        return 1;
    }
    if (!fdobades.getDescription(false).empty())
    {
        std::cerr << "### FATAL ERROR ### description (" << fdobades.getDescription(false) << ") != None" << std::endl;
        return 1;
    }
    else if (fdobades.getDescription() != description)
    {
        std::cerr << "### FATAL ERROR ### description (" << fdobades.getDescription() << ") != (" << description << ")" << std::endl;
        return 1;
    }
    if (!fdobades.getConfiguration(false).empty())
    {
        std::cerr << "### FATAL ERROR ### configuration (" << fdobades.getConfiguration(false) << ") != None" << std::endl;
        return 1;
    }
    else if (fdobades.getConfiguration() != configuration)
    {
        std::cerr << "### FATAL ERROR ### configuration (" << fdobades.getConfiguration() << ") != (" << configuration << ")" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    ierr = fdobades.extractRecordBinary("03/02/0103/01000000000000000001", true, true, false, 13937047830326616237u, 14899853969945466472u);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### accumulateBinary Second" << std::endl;
        return 1;
    }
    if (fdobades.getDescription(false) != description)
    {
        std::cerr << "### FATAL ERROR ### description (" << fdobades.getDescription(false) << ") != (" << description << ")" << std::endl;
        return 1;
    }
    if (fdobades.getDescription() != description)
    {
        std::cerr << "### FATAL ERROR ### description (" << fdobades.getDescription() << ") != (" << description << ")" << std::endl;
        return 1;
    }
    if (!fdobades.getConfiguration(false).empty())
    {
        std::cerr << "### FATAL ERROR ### configuration (" << fdobades.getConfiguration(false) << ") != None" << std::endl;
        return 1;
    }
    else if (fdobades.getConfiguration() != configuration)
    {
        std::cerr << "### FATAL ERROR ### configuration (" << fdobades.getConfiguration() << ") != (" << configuration << ")" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-" << std::endl;
    {
        // "03/02/0103/01000000000000000001"
        int z = 3;
        int ne = 2;
        std::vector<int> nl {1, 3};
        std::vector<int> nlq0{1, 0, 0, 0, 0, 0, 0, 0, 0, 1};
        std::vector<int> nlq1{1, 0, 0, 0, 0, 0, 0, 0, 0, 1};
        double forcOsc = 0.;
        std::vector<float> enerRy;
        std::vector<float> proba;
        std::vector<int> J21Ini;
        std::vector<int> J21Fin;
        float m1;
        float m2;
        float m3;
        bool newMcdfDB = false;

        ierr = fdobades.mcdf_for_saphyr(z, ne, nl, nlq0, nlq1, forcOsc, enerRy, proba, J21Ini, J21Fin, m1, m2, m3, newMcdfDB);
        if (ierr)
        {
            std::cerr << "### FATAL ERROR ### mcdf_for_saphyr" << std::endl;
            return 1;
        }
    }
    //-----------------------------------
    std::cout << "...Finish" << std::endl;
    //-----------------------------------
    return 0;
}
