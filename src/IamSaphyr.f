       subroutine IamSaphyr()

       real *8 Z, atmass
       integer ne, nLines, nLinesX
       integer n(21), l(21), nlq0(21), nlq1(21), nl0, nl1, nlS
       integer DemiGaussOut
       real*8 MoyG,SigG,MoyDG,SigDGM,SigDGP
       real *8 eLine(50000), pLine(50000), foa_mcdf
       logical NR, newMcdf

       integer in, il, i

c donnees utiles en entree
c on considere le fichier de la base 03/03/0103/010101
       Z=3
       ne=3
       i=1
       do in=1,6
          do il=0,in-1
             n(i)=in
             l(i)=il
             nlq0(i)=0
             nlq1(i)=0
             i=i+1
          enddo
       enddo

       nlq0(1)=1
       nlq0(2)=1
       nlq0(3)=1
       nlq1(1)=0
       nlq1(2)=1
       nlq1(3)=2
       nl0=1
       nl1=3
       nlS=3

       NR=false
       nLinesX=50000
       newMcdf=false

c donnees utiles en sortie (initialisee a -1, mais cela n'est pas utile)
       nLines=-1
       foa_mcdf=-1
       eLine=-1
       pLine=-1

c donnees inutiles
       atmass=-1



c Appel mcdf_data (comme si Saphyr le faisait lui-meme)
       CALL mcdf_data(Z,ne,atmass,n,l,nlq0,nlq1,nl0,nl1,nlS,NR
     +                      ,nLinesX,nLines,eLine,pLine,foa_mcdf
     +                      ,DemiGaussOut,MoyG,SigG,MoyDG,SigDGM,SigDGP
     +                      ,NewMCDF)

       end subroutine IamSaphyr




************************************************************************
      subroutine mcdf_data(Z,nbde,atmass,n,l,nlq0,nlq1,nl0,nl1,nlS,NR
     +                      ,nLinesX,nLines,eLine,pLine,foa_mcdf
     +                      ,DemiGaussOut,MoyG,SigG,MoyDG,SigDGM,SigDGP
     +                      ,NewMCDF)
*	****************************************************************
        implicit none
c input
        real*8 Z,atmass ! atmass is not used
        integer nbde,n(*),l(*),nlq0(*),nlq1(*),nl0,nl1,nlS,nLinesX
        logical NR
        !
        logical demiGauss_IN
        logical NewMCDF010000
c output
        integer nLines
        real*8 eLine(nLinesX),pLine(nLinesX),foa_mcdf
        integer DemiGaussOut
        real*8 MoyG,SigG,MoyDG,SigDGM,SigDGP
c local
        integer iZ010000
        character DBpath*46,Dok*7,Dxx*7,Dtmp*8,Fpath*11,Tpath*11
	parameter(DBpath='/.../'
     >		 ,Dok='mcDFdb/',Dxx='mcDFxx/',Dtmp='mcDFtmp/')
!	character DBpath*50,Dok*7,Dxx*7,Dtmp*8,Fpath*11,Tpath*11
!	parameter(DBpath=
!     >'/...//'
!     >		 ,Dok='mcDFdb/',Dxx='mcDFxx/',Dtmp='mcDFtmp/')
        character dataF*93,c2*2
        integer lF
        logical done,printWaitMessage
        integer system,i,iaux010000
        integer j_aux,jLine(nLinesX),ind(nLinesX)
        real*8 aux,e_aux,p_aux,sumLines
        integer mcdf_methX,mcdf_meth
        parameter(mcdf_methX=3)
        real*8 ryd
        parameter (ryd = 13.60580436d0)
        !
        real*8 SigDG0,DMOM1,DMOM2,DMOM3,MOM0,MOM1,MOM2,MOM3,MU2,VAR,M3
        real*8 R3DPI,KAP,PI,DPI,SIDPI
        REAL*8 THETA,DELTA,DVARSVAR,COEFSIG,DSIG,COEFD
        ! FICHIER ABSENT  include '../0_communs/com_path.h'
        real*8 Astd(99)010000
*	****************************************************************
        data Astd/1.00794 ,  4.0026  ,  6.941   ,  9.01218 , 10.81    ,
     6		 12.011   , 14.0067  , 15.9994  , 18.998403, 20.179   ,
     1		 22.98977 , 24.305   , 26.98154 , 28.0855  , 30.97376 ,
     6		 32.06    , 35.453   , 39.948   , 39.0983  , 40.08    ,
     2		 44.9559  , 47.88    , 50.9415  , 51.996   , 54.938   ,
     6		 55.847   , 58.9332  , 58.69    , 63.546   , 65.38    ,
     3		 69.72    , 72.59    , 74.9216  , 78.96    , 79.904   ,
     6		 83.8     , 85.4678  , 87.62    , 88.9059  , 91.22    ,
     4		 92.9064  , 95.94    , 98.      ,101.07    ,102.9055  ,
     6		106.42    ,107.8682  ,112.41    ,114.82    ,118.69    ,
     5		121.75    ,127.6     ,126.9045  ,131.29    ,132.9054  ,
     6		137.33    ,138.9055  ,140.12    ,140.9077  ,144.24    ,
     6		145.      ,150.36    ,151.96    ,157.25    ,158.9254  ,
     6		162.5     ,164.9304  ,167.26    ,168.9342  ,173.04    ,
     7		174.967   ,178.49    ,180.9479  ,183.85    ,186.207   ,
     6		190.2     ,192.22    ,195.08    ,196.9665  ,200.59    ,
     8		204.383   ,207.2     ,208.9804  ,209.      ,210.      ,
     6		222.      ,223.      ,226.0254  ,227.0278  ,232.0381  ,
     9		231.0359  ,238.0289  ,237.0482  ,244.      ,243.      ,
     6		247.      ,247.      ,251.      ,252.      /
*	****************************************************************
  666	format('-W- nLines>nLinesX : UTA/SOSA will be used (',a,')')
 2222	format(1pe12.6,1x,1pe12.6,1x,i3)
*	****************************************************************
c
c =====	init
c
	nLines=0
        iZ = nint(Z)
c path
	if(nl1.gt.99) then
	  print*,"-W- MCDF DB: unable to index active orbitals..."
	  pause
	  return
	endif
	Fpath=c2(iZ)//'/'//c2(nbde)//'/'//c2(nl0)//c2(nl1)//'/'
        Tpath=c2(iZ)//'_'//c2(nbde)//'_'//c2(nl0)//c2(nl1)//'_'
c file
	lF=0
	do i=1,nlS
	  dataF(lF+1:lF+2)=c2(nlq0(i))
	  lF=lF+2
	enddo
	if(NR) then
	  dataF(lF+1:lF+3)='.NR'
	  lF=lF+3
	endif
c
c =====	DB checks
c
c computed elsewhere?                                           ! 1 proc/task
         printWaitMessage=.true.
   01    inquire(file=DBpath//Dtmp//Tpath//dataF(1:lF),exist=done)
         if(done) then
           if(printWaitMessage) then
             write(6,'(a)') '  > waiting for file '//Tpath//dataF(1:lF)
             printWaitMessage=.false.
           endif 
           call sleep(1)
           goto 01
         endif
c already succeded?
	inquire(file=DBpath//Dok//Fpath//dataF(1:lF),exist=done)
	if(done) then
	  open(unit=1,file=DBpath//Dok//Fpath//dataF(1:lF),status='old')
	  goto 90
	endif
c already failed?
	inquire(file=DBpath//Dxx//Fpath//dataF(1:lF),exist=done)
	if(done) return

cosPhi140717>>
       !print*,' -W- no Xtra MCDF for new cases: UTA used (cosPhi140717)'
       if(.not.NewMCDF) return
cosPhi140717<<

c
c ===== new mcdf run(s)
c
        iaux=system('touch '//DBpath//Dtmp//Tpath//dataF(1:lF)) ! 1 proc/task

	mcdf_meth=0
   10	mcdf_meth=mcdf_meth+1
c input
	open(unit=1,file='mcdf_input.dat',status='unknown')
	write(1,*) 'input data file from spectreDCA'
      ! write(1,*) 'atom', Z, atmass, 0 ! no
	write(1,*) 'atom', Z, Astd(iZ), 0 ! yes
	if(NR) write(1,*) 'clum 1.e8'
	write(1,*) 'norb', nlS
	do i=1,nlS
	  write(1,'(2i3)') n(i),l(i)
	enddo
	write(1,*) 'conf 2'
	write(1,'(55i3)') nlq0(1:nlS)
	write(1,'(55i3)') nlq1(1:nlS)
	write(1,*) 'etrs'
	write(1,*) 'rgit'
	write(1,*) 'meth ',mcdf_meth
	write(1,*) 'fini'
	write(1,*) 'e:ev'
	write(1,*) 'e+fo'
        write(1,*) 'file 21 O fort.999'
	write(1,*) 'a:1r  1 -1'
	write(1,*) 'rail  1'
        write(1,*) 'brat  1'
	write(1,*) 'fini'
	close(1)
c run
	iaux=system('./mcdf.exe'
     <            //' < mcdf_input.dat'
     >            //' > '//path(1:lpath)//'mcdf_run.log')
c failed ?
	inquire(file='fort.999',exist=done)
	if(.not.done) then
	  write(6,'("-W- mcdf failed @ meth",i2)') mcdf_meth
	  if(mcdf_meth.lt.mcdf_methX) goto 10			! extraball
	  iaux=system('test -d '//DBpath//Dxx//Fpath(1:3))
	  if(iaux.ne.0) then
	    iaux=system('mkdir -m 770 '//DBpath//Dxx//Fpath(1:3))
	    iaux=system('chmod g+s '//DBpath//Dxx//Fpath(1:3))
	  endif
	  iaux=system('test -d '//DBpath//Dxx//Fpath(1:6))
	  if(iaux.ne.0) then
	    iaux=system('mkdir -m 770 '//DBpath//Dxx//Fpath(1:6))
	    iaux=system('chmod g+s '//DBpath//Dxx//Fpath(1:6))
	  endif
	  iaux=system('test -d '//DBpath//Dxx//Fpath     )
	  if(iaux.ne.0) then
	    iaux=system('mkdir -m 770 '//DBpath//Dxx//Fpath     )
	    iaux=system('chmod g+s '//DBpath//Dxx//Fpath     )
	  endif
	  iaux=system('touch '//DBpath//Dxx//Fpath//dataF(1:lF))
	  iaux=system('chmod 660 '//DBpath//Dxx//Fpath//dataF(1:lF))
          iaux=system('rm '//DBpath//Dtmp//Tpath//dataF(1:lF))  ! 1 proc/task
	  return						! game over
	endif
c output
        nLines=0
	sumLines=0.d0
	open(unit=1,file='fort.999',status='old')
   20	read(1,*) iaux,iaux,j_aux,iaux,aux,aux,e_aux,aux,p_aux
	if(j_aux.eq.0) goto 25
	  nLines=nLines+1
	  if(nLines.gt.nLinesX) then
	    close(1,status='DELETE')
	    nLines=0
	    write(6,666) 'MCDF data unstored'
            iaux=system('rm '//DBpath//Dtmp//Tpath//dataF(1:lF)) ! 1 proc/task
	    return
	  endif
	  jLine(nLines)=j_aux
	  eLine(nLines)=e_aux*2
	  aux=j_aux*p_aux
	  pLine(nLines)=aux
	  sumLines=sumLines+aux
	goto 20
   25	close(1,status='DELETE')
c arrange
	call indexx(nLines,eLine,ind)
c store
	iaux=system('test -d '//DBpath//Dok//Fpath(1:3))
	if(iaux.ne.0) then
	  iaux=system('mkdir -m 770 '//DBpath//Dok//Fpath(1:3))
	  iaux=system('chmod g+s '//DBpath//Dok//Fpath(1:3))
	endif
	iaux=system('test -d '//DBpath//Dok//Fpath(1:6))
	if(iaux.ne.0) then
	  iaux=system('mkdir -m 770 '//DBpath//Dok//Fpath(1:6))
	  iaux=system('chmod g+s '//DBpath//Dok//Fpath(1:6))
	endif
	iaux=system('test -d '//DBpath//Dok//Fpath     )
	if(iaux.ne.0) then
	  iaux=system('mkdir -m 770 '//DBpath//Dok//Fpath     )
	  iaux=system('chmod g+s '//DBpath//Dok//Fpath     )
	endif
c patch140523>>
	inquire(file=DBpath//Dok//Fpath//dataF(1:lF),exist=done)
	if(done) then
	  print*,"uh? file should not be here... waiting for 10 sec"
          call sleep(10)
	endif
c patch140523<<
	done=.false.
c	open(unit=1,file=DBpath//Dok//Fpath//dataF(1:lF),status='new')     !patch140523
	open(unit=1,file=DBpath//Dok//Fpath//dataF(1:lF),status='unknown') !patch140523
	aux=1.d0/sumLines
	if(NR) sumLines=(1.d8/137.03598950d0)*sumLines
	write(1,*) nLines,sumLines
	do i=1,nLines
	  iaux=ind(i)
	  write(1,2222) eLine(iaux),aux*pLine(iaux),jLine(iaux)
	enddo
        iaux=system('rm '//DBpath//Dtmp//Tpath//dataF(1:lF))    ! 1 proc/task
	rewind(1)

c
c ===== read data from mcdf
c
   90	read(1,*) nLines,foa_mcdf
c
c >>> Xtine : double demi-gaussienne : 01/03/2019
c
        PI=ACOS(-1.)
        DPI=PI-3.
        SIDPI=6.*DPI
        R3DPI=SQRT(3.*DPI)
        COEFD=SQRT(PI/SIDPI)
        COEFSIG=(3.*PI-8.)/SIDPI
        DemiGaussOut = 0
        MoyG=0.d0
        SigG=0.d0
        MoyDG=0.d0
        SigDG0=0.d0
        SigDGM=0.d0
        SigDGP=0.d0
        MOM0=0.d0
        MOM1=0.d0
        MOM2=0.d0
        MOM3=0.d0
	if( (nLines.eq.0).or.(foa_mcdf.eq.0.d0) ) then
	  nLines=0
	  write(6,666) 'MCDF data unused'
	  return
	endif
	if(nLines.gt.nLinesX) then
	  nLines=0
	  write(6,666) 'MCDF data unused'
	  return
	endif
	do i=1,nLines
	  read(1,2222,err=1234) eLine(i),pLine(i),iaux
          DMOM1=eLine(i)*pLine(i)
          DMOM2=eLine(i)*DMOM1
          DMOM3=eLine(i)*DMOM2
          MOM0=MOM0+pLine(i)
          MOM1=MOM1+DMOM1
          MOM2=MOM2+DMOM2
          MOM3=MOM3+DMOM3
	enddo
        MoyG=MOM1/MOM0
        MU2=MoyG*MoyG
        VAR=MOM2/MOM0-MU2
        IF(VAR.LT.0.D0) THEN
          DemiGaussOut = 0
          MoyG=0.d0
          SigG=0.d0
          MoyDG=0.d0
          SigDG0=0.d0
          SigDGM=0.d0
          SigDGP=0.d0
          GOTO 1234
        END IF
        SigG=SQRT(VAR)
        M3=MOM3/MOM0-MoyG*(3.*VAR+MU2)
        IF (6.75D0*DPI*M3*M3.LT.VAR*VAR*VAR) THEN
          DemiGaussOut = 2
          THETA=ACOS(-1.5*R3DPI*M3/VAR/SigG)
          KAP=COS((THETA-2.*PI)/3.)
          DELTA=2./R3DPI*SigG*KAP
          MoyDG=MoyG-DELTA
          DVARSVAR=COEFSIG*KAP*KAP
          IF(DVARSVAR.GT.1.D0) THEN
            DemiGaussOut = 0
            MoyG=0.d0
            SigG=0.d0
            MoyDG=0.d0
            SigDG0=0.d0
            SigDGM=0.d0
            SigDGP=0.d0
            GOTO 1234
          END IF
          SigDG0=SigG*SQRT(1.-DVARSVAR)
          DSIG=SigG*COEFD*KAP
          SigDGM=SigDG0-DSIG
          SigDGP=SigDG0+DSIG
          IF ((SigDGM.LT.0.).OR.(SigDGP.LT.0.)) THEN !' SigDGM ou SigDGP NEGATIF, ON PREND UNE GAUSSIENNE (MoyG, SigG)'
            DemiGaussOut = 1
            MoyDG=MoyG
            SigDG0=SigG
            SigDGM=SigG
            SigDGP=SigG
          END IF
        ELSE ! ' DISCRIMINANT POSITIF, ON PREND UNE GAUSSIENNE (MoyG, SigG)'
          DemiGaussOut = 1
          MoyDG=MoyG
          SigDG0=SigG
          SigDGM=SigG
          SigDGP=SigG
        END IF
 1234   close(1)
c
c <<< Xtine : double demi-gaussienne : 01/03/2019
c
	if(.not.done)
     >	  iaux=system('chmod 660 '//DBpath//Dok//Fpath//dataF(1:lF))
c
c ===== the end
c
	return
	end
************************************************************************
	function c2(i2)
*	****************************************************************
	implicit none
	character*2 c2
	integer i1,i2
*	****************************************************************
	if((i2.lt.0).or.(i2.gt.99)) then
	  write(6,'("-E- function c2 called with i2=",i8)') i2
	  stop
	endif
	i1=mod(i2,10)
	c2(2:2)=char(48+i1)
	i1=(i2-i1)/10
	c2(1:1)=char(48+i1)
	return
	end
