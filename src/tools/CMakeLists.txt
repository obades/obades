file(GLOB src_ascii_to_binary ascii_to_binary.cpp) # ../../external/simdjson/simdjson.cpp)

add_executable(ascii_to_binary ${src_ascii_to_binary})

target_include_directories(ascii_to_binary 
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/nlohmann>  # copy from nlohmann_json::nlohmann_json
    # PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/simdjson>
    PUBLIC     $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)



file(GLOB src_extract_McdfDb extract_McdfDb.cpp) # ../../external/simdjson/simdjson.cpp)

add_executable(extract_McdfDb ${src_extract_McdfDb})

target_include_directories(extract_McdfDb 
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/nlohmann>  # copy from nlohmann_json::nlohmann_json
    # PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/simdjson>
    PUBLIC     $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)



file(GLOB src_extract_McdfDescription extract_McdfDescription.cpp) # ../../external/simdjson/simdjson.cpp)

add_executable(extract_McdfDescription ${src_extract_McdfDescription})

target_include_directories(extract_McdfDescription
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/nlohmann>  # copy from nlohmann_json::nlohmann_json
    # PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/simdjson>
    PUBLIC     $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)



file(GLOB src_extract_McdfParameters extract_McdfParameters.cpp) # ../../external/simdjson/simdjson.cpp)

add_executable(extract_McdfParameters ${src_extract_McdfParameters})

target_include_directories(extract_McdfParameters
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/nlohmann>  # copy from nlohmann_json::nlohmann_json
    # PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/simdjson>
    PUBLIC     $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)



file(GLOB src_list_Mcdf list_Mcdf.cpp) # ../../external/simdjson/simdjson.cpp)

add_executable(list_Mcdf ${src_list_Mcdf})

target_include_directories(list_Mcdf
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
    PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/nlohmann>  # copy from nlohmann_json::nlohmann_json
    # PRIVATE    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/external/simdjson>
    PUBLIC     $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
        