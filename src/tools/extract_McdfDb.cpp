#include "obades.hpp"

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int main(int _argc, const char *_argv[])
{
    std::cerr << "argc: " << _argc << std::endl;
    std::cerr << "argv: " << _argv[0] << std::endl;
    //-----------------------------------
    std::string name_record = "03/02/0103/010000000000000000010000000000000000000000";
    if (_argc == 2)
    {
        name_record = _argv[1];
    }
    //-----------------------------------
    std::string path_write = "temporary"; // accumulateBinary
    //-----------------------------------
    obades::obades fdobades("", path_write);
    //-----------------------------------
    std::string new_path_write = "temporary/extract";
    //-----------------------------------
    int ierr = fdobades.extractRecordBinary(name_record, true, false, false);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### extractRecordBinary" << std::endl;
        return 1;
    }
    //-----------------------------------
    ierr = fdobades.writePathAscii(new_path_write, name_record);
    if (ierr)
    {
        std::cerr << "### FATAL ERROR ### writeAscii" << std::endl;
        return 1;
    }
    //-----------------------------------
    std::cerr << "... terminated." << std::endl;
    return 0;
}
