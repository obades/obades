#include "obades.hpp"

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int main(int _argc, const char *_argv[])
{
    bool verbose = false;
    bool warning = true;
    //-----------------------------------
    std::cerr << "In progress ascii_to_binary..." << std::endl;
    //-----------------------------------
    if (verbose) std::cout << "argc: " << _argc << std::endl;
    if (verbose) std::cout << "argv: " << _argv[0] << std::endl;
    //-----------------------------------
    std::string path_read = "data/ascii/";
    std::string path_write = "temporary";
    //-----------------------------------
    {
        std::map<std::string, std::map< std::string, int> > mapAvanc;
        int ivaAnc = 0;
        //-----------------------------------
        obades::obades fdobades(path_read, path_write);
        //-----------------------------------
        const std::filesystem::path ascii{path_read};
        for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{ascii}) 
        {
            if(!dir_entry.is_directory())
            {
                std::string name_record = dir_entry.path().string().substr(path_read.length());
                //-----------------------------------
                // reject name file beginning by mcdf.don_
                std::string::size_type pos = name_record.rfind('/');
                if (pos == std::string::npos)
                {
                    if(name_record.find("mcdf.don_") != std::string::npos)
                    {
                        if (verbose) std::cerr << "Ignored " << name_record << std::endl;
                        continue;
                    }
                    if(name_record.find("tableIndexNe") != std::string::npos)
                    {
                        if (verbose) std::cerr << "Ignored " << name_record << std::endl;
                        continue;
                    }
                } else {
                    if(name_record.substr(pos+1).find("mcdf.don_") != std::string::npos)
                    {
                        if(verbose) std::cerr << "Ignored " << name_record << std::endl;
                        continue;
                    }
                    if(name_record.substr(pos+1).find("tableIndexNe") != std::string::npos)
                    {
                        if (verbose) std::cerr << "Ignored " << name_record << std::endl;
                        continue;
                    }
                }
                //-----------------------------------    
                if (verbose) std::cout << "Treatment path: " << name_record << std::endl;
                //-----------------------------------
                std::string strZ = name_record.substr(0, 2);     
                std::string strNE = name_record.substr(3, 2);     
                mapAvanc[strZ][strNE]++;
                if (mapAvanc[strZ][strNE] % 10 == 0)
                {
                    if (warning)
                    {
                        std::cerr << "Z = " << strZ << "   ne = " << strNE << "   Avanc = " << std::setw(5) << mapAvanc[strZ][strNE] << std::endl;
                    }
                }
                //-----------------------------------    
                if (verbose) std::cout << "In progress..." << std::endl;
                //-----------------------------------    
                int ierr = fdobades.readAscii(name_record);
                if(ierr)
                {
                    std::cerr << "### FATAL ERROR ### readAscii" << std::endl;
                    return 1;
                }
                //-----------------------------------
                ierr = fdobades.accumulateRecordBinary(name_record);
                if(ierr)
                {
                    std::cerr << "### FATAL ERROR ### accumulateBinary" << std::endl;
                    return 1;
                }            
                //-----------------------------------    
                if (verbose) std::cerr << "... terminated." << std::endl;
            }
        }
    }
    //-----------------------------------
    std::cout << "...Finish" << std::endl;
    //-----------------------------------
    return 0;
}
