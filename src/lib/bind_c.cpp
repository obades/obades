#include <string.h>
#include "obades.hpp"

typedef void * OpaqueObject;

/// Functions connexion for binding C ptr -> C++ class
extern "C" {
   OpaqueObject
   GetObject(const std::string &_path_read, const std::string &_path_write, const bool _trace = true);

   int  
   ReadAscii(OpaqueObject foo, const std::string &_filename);

   int 
   GetForcOsc(OpaqueObject foo, double &_forcOsc);

   int 
   GetNumberOfRaies(OpaqueObject foo, int &_nbraies);

   int 
   GetRaie(OpaqueObject foo, size_t _index, float &_enerRy, float &_proba, unsigned char &_J21Ini, unsigned char &_J21Fin);

   void
   GetMcdf(
      OpaqueObject foo,
      const int  *_z,
      const int  *_ne,
      const int  *_nl,
      const int  *_s_nlq0,
      const int  *_nlq0,
      const int  *_s_nlq1,
      const int  *_nlq1,
      double     *_forcOsc,
      int        *_s_nbrays,
      float     **_enerRy,
      float     **_proba,
      int       **_J21Ini,
      int       **_J21Fin,
      float      *_m1,
      float      *_m2,
      float      *_m3,
      const int  *_newMcdfDb,
      int        *_ierr
   );

   void 
   DeleteObject(OpaqueObject foo);
}

OpaqueObject
GetObject(const std::string &_path_read, const std::string &_path_write, const bool _trace) {

   obades::obades *inst = new obades::obades(_path_read, _path_write, _trace);
   return (OpaqueObject)inst;
}

int 
ReadAscii(OpaqueObject foo, const std::string &_filename) {

   obades::obades *inst = (obades::obades *)foo;
   return inst->readAscii(_filename.c_str());
}

int 
GetNumberOfRaies(OpaqueObject foo, int &_nbraies) {

   obades::obades *inst = (obades::obades *)foo;
   _nbraies = inst->getNumberOfRaies();
   return 1;
}

int 
GetForcOsc(OpaqueObject foo, double &_forcOsc) {

   obades::obades *inst = (obades::obades *)foo;
   return inst->getForcOsc(_forcOsc);
}

int 
GetRaie(OpaqueObject foo, size_t _index, float &_enerRy, float &_proba, unsigned char &_J21Ini, unsigned char &_J21Fin) {

   obades::obades *inst = (obades::obades *)foo;
   return inst->getRaie(_index, _enerRy, _proba, _J21Ini, _J21Fin);
}

void
GetMcdf(
   OpaqueObject foo,
   const int  *_z,
   const int  *_ne,
   const int  *_nl,
   const int  *_s_nlq0,
   const int  *_nlq0,
   const int  *_s_nlq1,
   const int  *_nlq1,
   double     *_forcOsc,
   int        *_s_nbrays,
   float     **_enerRy,
   float     **_proba,
   int       **_J21Ini,
   int       **_J21Fin,
   float      *_m1,
   float      *_m2,
   float      *_m3,
   const int  *_newMcdfDb,
   int        *_ierr
) {
   obades::obades *inst = (obades::obades *)foo;

   inst->mcdf_for_saphyr_binding_c(
      _z, _ne, _nl, _s_nlq0, _nlq0, _s_nlq1, _nlq1, _forcOsc, _s_nbrays, _enerRy, _proba, _J21Ini,
      _J21Fin, _m1, _m2, _m3, _newMcdfDb, _ierr);
}

void 
DeleteObject(OpaqueObject foo) {

   obades::obades *inst = (obades::obades *)foo;
   delete(inst);
   return;
}
