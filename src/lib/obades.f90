!< module pour utiliser OBaDES depuis du fortran 
module obades

   use, intrinsic :: iso_c_binding
   implicit none
   private

   interface

      !! function connexion internal -> C

      !! create instance
      function GetObject(path_r, path_w, trace) result( optr ) bind (C, name="GetObject")

         use iso_c_binding
         implicit none

         ! Argument list
         character(len=*, kind=c_char), intent(in)         :: path_r
         character(len=*, kind=c_char), intent(in)         :: path_w
         logical  (c_bool),             intent(in), value  :: trace

         ! Function result
         type(c_ptr) :: optr

      end function

      !! read ascii file
      subroutine Load_f(optr, filename ) bind(C, name="ReadAscii")

         use iso_c_binding
         implicit none

         ! Argument list
         type     (c_ptr),              intent(in), value  :: optr
         character(len=*, kind=c_char), intent(in)         :: filename

      end subroutine 

      !! Get a energy and proba values
      subroutine GetForcOsc_f(optr, forcOsc ) bind(C, name="GetForcOsc")

         use iso_c_binding
         implicit none

         ! Argument list
         type     (c_ptr),              intent(in), value  :: optr
         real     (c_double),           intent(out)        :: forcOsc

      end subroutine

      !! Get a energy and proba values
      subroutine GetNumberOfRaies_f(optr, nbraies) bind(C, name="GetNumberOfRaies")

         use iso_c_binding
         implicit none

         ! Argument list
         type     (c_ptr),              intent(in), value  :: optr
         integer  (c_int),              intent(out)        :: nbraies

      end subroutine

      !! Get a energy and proba values
      subroutine GetRaie_f(optr, idx, energy, proba, j21ini, j21fin) bind(C, name="GetRaie")

         use iso_c_binding
         implicit none

         ! Argument list
         type    (c_ptr),           intent(in), value  :: optr
         integer (c_int),           intent(in), value  :: idx
         real    (c_float),         intent(out)        :: energy
         real    (c_float),         intent(out)        :: proba
         integer (c_signed_char),   intent(out)        :: j21ini
         integer (c_signed_char),   intent(out)        :: j21fin

      end subroutine

      !! Get information
      subroutine GetMcdf_f( optr, z, ne , nl, snlq0, nlq0, snlq1, nlq1,&
&        forcOsc, nbRays, enerRy, proba, J21Ini, J21Fin,&
&        m1, m2, m3, newMcdfDB, ierr) bind(c, name="GetMcdf")

         use, intrinsic :: iso_c_binding
         implicit none

         type(c_ptr), intent(in), value  :: optr
         integer(c_int)    :: z
         integer(c_int)    :: ne
         integer(c_int)    :: nl(2)
         integer(c_int)    :: snlq0
         integer(c_int)    :: nlq0(snlq0)
         integer(c_int)    :: snlq1
         integer(c_int)    :: nlq1(snlq1)
         real   (c_double) :: forcOsc
         integer(c_int)    :: nbRays
         type   (c_ptr)    :: enerRy
         type   (c_ptr)    :: proba
         type   (c_ptr)    :: J21Ini
         type   (c_ptr)    :: J21Fin
         real   (c_float)  :: m1
         real   (c_float)  :: m2
         real   (c_float)  :: m3
         integer(c_int)    :: newMcdfDB
         integer(c_int)    :: ierr

      end subroutine

      subroutine DeleteObject( optr ) bind(C, name="DeleteObject")

         use iso_c_binding
         implicit none

         ! Argument list
         type(c_ptr),                   intent(in), value  :: optr

      end subroutine

   end interface

   ! Object connexion to C pointer
   type(c_ptr), save :: obj = c_null_ptr

   !! list of available routine from module loaded
   public :: Create
   public :: Load
   public :: GetForcOsc
   public :: GetNumberOfRaies
   public :: GetRaie
   public :: GetMcdf
   public :: GetMcdfClean
   public :: Destroy

contains

   subroutine error_ptr_associated()
      implicit none

      print*, "ERROR, C pointer object is not created. Call 'Create' function."
      stop

      return

   end subroutine

   subroutine Create(path_r, path_w, trace)

      character(len=*, kind=c_char),    intent(in)         :: path_r
      character(len=*, kind=c_char),    intent(in)         :: path_w
      logical  (c_bool),                intent(in), value  :: trace

      if ( c_associated(obj) ) then
         call DeleteObject( obj )
      end if

      obj = GetObject(path_r=path_r, path_w=path_w, trace=trace)

      return

   end subroutine 

   subroutine Load(filename)

      character(len=*, kind=c_char),    intent(in)         :: filename

      if ( c_associated(obj) ) then
         call Load_f(obj, filename=filename)
      else
         call error_ptr_associated()
      end if

      return

   end subroutine

   subroutine GetForcOsc(idx, forcOsc)

      integer  (c_int),              intent(in), value     :: idx
      real     (c_double),           intent(out)           :: forcOsc
      
      if ( c_associated(obj) ) then
         call GetForcOsc_f(obj, forcOsc=forcOsc)
      else
         call error_ptr_associated()
      end if

      return

   end subroutine

   subroutine GetNumberOfRaies(idx, nbraies)

      integer  (c_int),              intent(in), value     :: idx
      integer  (c_int),              intent(out)           :: nbraies
      
      if ( c_associated(obj) ) then
         call GetNumberOfRaies_f(obj, nbraies=nbraies)
      else
         call error_ptr_associated()
      end if

      return

   end subroutine

   subroutine GetRaie(idx, energy, proba, j21ini, j21fin)

      integer  (c_int),         intent(in), value     :: idx
      real     (c_float),       intent(out)           :: energy
      real     (c_float),       intent(out)           :: proba
      integer(c_signed_char),   intent(out)           :: j21ini
      integer(c_signed_char),   intent(out)           :: j21fin

      if ( c_associated(obj) ) then
         call GetRaie_f(obj, idx=idx, energy=energy, proba=proba, j21ini=j21ini, j21fin=j21fin)
      else
         call error_ptr_associated()
      end if

      return

   end subroutine

   subroutine GetMcdf(z, ne, nl, nlq0, nlq1, forcOsc, enerRy, proba, J21Ini,&
&     J21Fin, m1, m2, m3, newMcdfDB_bool, ierr)

      use, intrinsic :: iso_c_binding
      implicit none

      ! Argument list
      integer(c_int)             :: z
      integer(c_int)             :: ne
      integer(c_int)             :: nl(2)
      integer(c_int)             :: nlq0(:)
      integer(c_int)             :: nlq1(:)
      real   (c_double)          :: forcOsc
      real   (c_float), pointer  :: enerRy(:)
      real   (c_float), pointer  :: proba(:)
      integer(c_int)  , pointer  :: J21Ini(:)
      integer(c_int)  , pointer  :: J21Fin(:)
      real   (c_float)           :: m1
      real   (c_float)           :: m2
      real   (c_float)           :: m3
      logical                    :: newMcdfDB_bool
      integer(c_int)             :: ierr

      integer(c_int)             :: newMcdfDB
      integer(c_int)             :: OUTnbrays
      type   (c_ptr)             :: OUTenerRy
      type   (c_ptr)             :: OUTproba
      type   (c_ptr)             :: OUTJ21Ini
      type   (c_ptr)             :: OUTJ21Fin

      if ( newMcdfDB_bool ) then
         newMcdfDB = 1
      else
         newMcdfDB = 0
      end if 
      
      print *, "GETMCDF IN +-+-+-+-+-+-+ GetMcdf +-+-+-+-+-+-+"
      print *, "GETMCDF IN  z = ", z
      print *, "GETMCDF IN  ne = ", ne
      print *, "GETMCDF IN  nl = [", nl(1), " ; ", nl(2), "]"
      print *, "GETMCDF IN  nlq0 = [", nlq0(1), " ; ", nlq0(2), " ... ]"
      print *, "GETMCDF IN  nlq1 = [", nlq1(1), " ; ", nlq1(2), " ... ]"
      print *, "GETMCDF IN  newMcdfDB = ", newMcdfDB
      print *, "GETMCDF IN  enerRy = ", size(enerRy)
      print *, "GETMCDF IN  proba = ", size(proba)
      print *, "GETMCDF IN  J21Ini = ", size(J21Ini)
      print *, "GETMCDF IN  J21Fin = ", size(J21Fin)
      print *, "GETMCDF IN +-+-+-+-+-+-+ GetMcdf +-+-+-+-+-+-+"

      if ( c_associated(obj) ) then
         call GetMcdf_f(obj, z=z, ne=ne, nl=nl, snlq0=size(nlq0), nlq0=nlq0,&
&            snlq1=size(nlq1), nlq1=nlq1,&
&            forcOsc=forcOsc, nbRays=OUTnbrays, enerRy=OUTenerRy, proba=OUTproba,&
&            J21Ini=OUTJ21Ini, J21Fin=OUTJ21Fin,m1=m1, m2=m2, m3=m3, newMcdfDB=newMcdfDB, ierr=ierr)

         print *, "GETMCDF OUT +-+-+-+-+-+-+ GetMcdf +-+-+-+-+-+-+"
         print *, "GETMCDF OUT  nbRays = ", OUTnbrays
         print *, "GETMCDF OUT +-+-+-+-+-+-+ GetMcdf +-+-+-+-+-+-+"

         call c_f_pointer( OUTenerRy, enerRy, [OUTnbrays] )
         call c_f_pointer( OUTproba, proba, [OUTnbrays] )
         call c_f_pointer( OUTJ21Ini, J21Ini, [OUTnbrays] )
         call c_f_pointer( OUTJ21Fin, J21Fin, [OUTnbrays] )
      else
         call error_ptr_associated()
      end if

      return

   end subroutine

   subroutine GetMcdfClean(enerRy, proba, J21Ini, J21Fin)

      use, intrinsic :: iso_c_binding
      implicit none

      ! Argument list
      real   (c_float), pointer  :: enerRy(:)
      real   (c_float), pointer  :: proba(:)
      integer(c_int)  , pointer  :: J21Ini(:)
      integer(c_int)  , pointer  :: J21Fin(:)

      print *, "GETMCDF OUT +-+-+-+-+-+-+ GetMcdfClean +-+-+-+-+-+-+"
      print *, "GETMCDF OUT  Deallocate"
      print *, "GETMCDF OUT +-+-+-+-+-+-+ GetMcdfClean +-+-+-+-+-+-+"

      deallocate(enerRy)
      deallocate(proba)
      deallocate(J21Ini)
      deallocate(J21Fin)

      return

   end subroutine

   subroutine Destroy()

      if ( c_associated(obj) ) then
         call DeleteObject( obj )
         obj = c_null_ptr
      end if

      return

   end subroutine 

end module
