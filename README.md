# OBaDES

Lecture/ecriture de fichiers json à partir de sources C++.

## Prendre le projet

```bash
git clone https://gitlab.com/obades/obades.git
```

## Configurer et compiler

```bash
mkdir build ; cd build
cmake ..  # configuration
make      # compilation
```

Ces étapes nécessitent la présence des compilateurs g++-&é, gcc-12 et gfortran-12.

## Options de configuration

[CMake](https://cmake.org/cmake/help/latest/index.html) peut etre exécuter avec des options.  
Voici l'option du projet:

OBADES_ENABLE_DOC : OFF (default), ON.

## Executer

Il existe deux variables d'environnement positionnable avant d'instancier une classe obades :
- OBADES_TRACE "0" désactive les traces (défaut), autres valeurs activent les traces
- OBADES_ASCII_SUBDIRDON indique le nom du sous répertoire où trouver le fichier .don, par défaut "mcdfDon"
- OBADES_ACCUMLATED_BINARY_BY_NE "0" active le mode d'écriture dans un unique fichier binaire, autres valeurs
  activent le mode de regroupement par fichier (Z, Ne) (défaut)
- OBADES_LIMIT_SIZE_FILE indique la taille à partir de laquelle le fichier binaire doit être découpé,
  cette taille s'exprime en octets (par défaut 536870912000 soit 500Go)

## Collaborer

Après avoir développemer dans le projet, merci de faire (au minimum) une branche [git](https://git-scm.com/doc) et une [merge request](https://gitlab.com/obades/obades/-/merge_requests).

## Tester

Le lancement des tests se fait via l'option standard CMake (`-DBUILD_TESTING=ON`).

Le test **main** comporte la validation de l'API C++.
L'emploi des checksums met en avant qu'une écriture / lecture en ASCII peut modifier à la marge les valeurs des réelles,
ce qui n'est pas le cas en écriture / lecture en binaire.
